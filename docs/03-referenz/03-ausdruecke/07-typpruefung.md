# Ausdrücke für Typprüfung


## ISBOOLEAN

```
ISBOOLEAN <string>
```

Liefert `TRUE`, falls \<string> einen Booleanwert enthält, ansonsten `FALSE`.


```
ISBOOLEAN <liste>
```

Boolean-Prüfung für alle Elemente einer Liste. 


::: details Beispiel
```
SET wert "TRUE"
>> /tell Kugelschreiber [wert] >> [=ISBOOLEAN [wert]]
SET wert "FOO"
>> /tell Kugelschreiber [wert] >> [=ISBOOLEAN [wert]]
SET wert "A 1 1.0 0.2E2 TRUE FALSE HANNES"
>> /tell Kugelschreiber [wert] >> [=ISBOOLEAN [wert]]
```

*Zugehörige Ausgabe:*
```
kulipuptest: TRUE >> TRUE
kulipuptest: FOO >> FALSE
kulipuptest: A 1 1.0 0.2E2 TRUE FALSE HANNES >> FALSE FALSE FALSE FALSE TRUE TRUE FALSE
```
:::



## ISINTEGER

```
ISINTEGER <string>
```

Liefert `TRUE`, falls \<string> einen Integerwert enthält, ansonsten `FALSE`. 


```
ISINTEGER <liste>
```

Integer-Prüfung für alle Elemente einer Liste. 


::: details Beispiel
```
SET wert "1"
>> /tell Kugelschreiber [wert] >> [=ISINTEGER [wert]]
SET wert "2.0"
>> /tell Kugelschreiber [wert] >> [=ISINTEGER [wert]]
SET wert "FOO"
>> /tell Kugelschreiber [wert] >> [=ISINTEGER [wert]]
SET wert "1 1.5 1.0000 FOO 1E3 ;-)"
>> /tell Kugelschreiber [wert] >> [=ISINTEGER [wert]]
```

*Zugehörige Ausgabe:*
```
kulipuptest: 1 >> TRUE
kulipuptest: 2.0 >> FALSE
kulipuptest: FOO >> FALSE
kulipuptest: 1 1.5 1.0000 FOO 1E3 ;-) >> TRUE FALSE FALSE FALSE FALSE FALSE
```
:::



## ISFLOAT

```
ISFLOAT <string>
```

Liefert `TRUE`, falls \<string> einen Floatwert enthält, ansonsten `FALSE`. 

```
ISFLOAT <liste>
```

Float-Prüfung für alle Elemente einer Liste. 


::: details Beispiel
```
SET wert "1"
>> /tell Kugelschreiber [wert] >> [=ISFLOAT [wert]]
SET wert "2.0"
>> /tell Kugelschreiber [wert] >> [=ISFLOAT [wert]]
SET wert "0.789E3"
>> /tell Kugelschreiber [wert] >> [=ISFLOAT [wert]]
SET wert "FOO"
>> /tell Kugelschreiber [wert] >> [=ISFLOAT [wert]]
SET wert "1 1.5 1.0000 FOO 0.123E4 1E3 ;-)"
>> /tell Kugelschreiber [wert] >> [=ISFLOAT [wert]]
```

*Zugehörige Ausgabe:*
```
kulipuptest: 1 >> FALSE
kulipuptest: 2.0 >> TRUE
kulipuptest: 0.789E3 >> TRUE
kulipuptest: FOO >> FALSE
kulipuptest: 1 1.5 1.0000 FOO 0.123E4 1E3 ;-) >> FALSE TRUE TRUE FALSE TRUE FALSE FALSE
```
:::



## ISNUMBER

```
ISNUMBER <string>
```

Liefert `TRUE`, falls \<string> einen Integerwert oder Floatwert enthält,ansonsten `FALSE`. 

```
ISNUMBER <liste>
```

Integer/Float-Prüfung für alle Elemente einer Liste. 


::: details Beispiel
```
SET wert "3 x 4.0"
>> /tell Kugelschreiber [wert] >> [=ISNUMBER [wert]]
```

*Zugehörige Ausgabe:*
```
kulipuptest: 3 x 4.0 >> TRUE FALSE TRUE
```
:::
