# Ausdrücke für Zahlen

## Wertbelegung

```
- <int>
+ <int>
```

Unäres Minus/Plus (=Vorzeichen). 



## Berechnung

```
<int> + <int>
<int> - <int>
<int> * <int>
<int> / <int>
<int> % <int>
<int> ^ <int>
```

Liefert das entsprechende Ergebnis. Das Puppet beherrscht die Punkt-vor-Strich-Regel.

- Addition
- Subtraktion
- Multiplikation
- Division
- Modulo
- Exponentialrechnung



## Vergleichende Ausdrücke

```
<int> < <int>
<int> > <int>
<int> <= <int>
<int> >= <int>
```

Liefert `TRUE`, falls die erste Zahl kleiner / größer / kleinergleich / größergleich als die zweite ist, ansonsten `FALSE`.



## Zufallszahlen

```
[RND<zahl>]
```

Liefert eine (Integer) Zufallszahl zwischen 0 und ( \<zahl> - 1 ).


```
[RND]
[RND0]
```

Liefert eine (Float) Zufallszahl zwischen 0 und 1.


::: details Beispiel
```
# Hier wird eine Zufallszahl 0, 1, 2, 3 oder 4 geliefert:
[RND5]
# SLC: 10000 divided by random number between 0 and 1
10000 / [RND0] = 107041.63360691446
```
:::
