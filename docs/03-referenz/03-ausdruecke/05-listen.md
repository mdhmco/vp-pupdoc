# Ausdrücke für Listen


## Komma-Operator

```
<liste> , <liste>
```

Der Komma-Operator verknüpft (wie der [ADDLIST-Befehl](../02-befehle/09-listen#addlist-befehl)) zwei oder mehr Listen (oder Elemente). 

  
::: details Beispiel
```
EVAL liste = 1 TO 4, 6 TO 10 STEP 2
>> /tell Kugelschreiber liste >> [=[liste]]
EVAL liste = 5 TO 7, 1 TO 9 STEP 2, 21 DOWNTO 13 STEP 3
>> /tell Kugelschreiber liste >> [=[liste]]
```

*Zugehörige Ausgabe:*
```
kulipuptest: liste >> 1 2 3 4 6 8 10
kulipuptest: liste >> 5 6 7 1 3 5 7 9 21 18 15
```
:::



### Probleme nach Einführung des Komma-Operators

::: warning Warnung
Durch diese Änderung ergaben sich Probleme bei der Anwendung des [SET-Befehls](../02-befehle/07-variablen#set-befehl) zur Erzeugung einer Liste mit mehreren - durch Komma getrennten - Elementen ohne Verwendung von Hochkommata.
:::


```
# - problematisch -
SET LISTE1 [SUBST1],[SUBST2],[SUBST3]
# - so müsste es aussehen -
SET LISTE1 "[SUBST1],[SUBST2],[SUBST3]"
```

> Quelle: BSW Puppet Forum >> Index-Listen/Permutationen / Komma-Operator - Antwort #4



## TO / DOWNTO und STEP

```
<von> TO <bis>
<von> TO <bis> STEP <schritt>
```

Liefert eine Liste mit den Werten \<von> bis \<bis> (aufsteigend); evtl. unter Angabe der Schrittweite. Dies kann auf `INTEGER` oder `FLOAT` Werte angewendet werden. 


```
<von> DOWNTO <bis>
<von> DOWNTO <bis> STEP <schritt>
```

Liefert eine Liste mit den Werten \<von> bis \<bis> (absteigend); evtl. unter Angabe der Schrittweite. Dies kann auf `INTEGER` oder `FLOAT` Werte angewendet werden. 


```
<von> TO <bis> DOWNTO <bis>
<von> TO <bis> STEP <schritt> DOWNTO <bis>
<von> DOWNTO <bis> TO <bis>
<von> DOWNTO <bis> STEP <schritt> TO <bis>
```

Auch Kombinationen sind möglich.


::: details Beispiel
```
# INTEGER
EVAL ausdruck = "1 TO 10"
>> /tell Kugelschreiber [ausdruck] >> [=[ausdruck]]
EVAL ausdruck = "1 TO 20 STEP 3"
>> /tell Kugelschreiber [ausdruck] >> [=[ausdruck]]
EVAL ausdruck = "12 DOWNTO 3"
>> /tell Kugelschreiber [ausdruck] >> [=[ausdruck]]
EVAL ausdruck = "150 DOWNTO 10 STEP 20"
>> /tell Kugelschreiber [ausdruck] >> [=[ausdruck]]
EVAL ausdruck = "150 DOWNTO 10 STEP 20 TO 14"
>> /tell Kugelschreiber [ausdruck] >> [=[ausdruck]]
EVAL ausdruck = "150 DOWNTO 140 TO 145"
>> /tell Kugelschreiber [ausdruck] >> [=[ausdruck]]
# FLOAT
EVAL ausdruck = "1.0 TO 10.0"
>> /tell Kugelschreiber [ausdruck] >> [=[ausdruck]]
EVAL ausdruck = "1.0 TO 10.0 STEP 2"
>> /tell Kugelschreiber [ausdruck] >> [=[ausdruck]]
EVAL ausdruck = "5.0 TO 10.0 DOWNTO 7.0"
>> /tell Kugelschreiber [ausdruck] >> [=[ausdruck]]
EVAL ausdruck = "1.0 TO 11.0 STEP 3.0 DOWNTO 7.0"
>> /tell Kugelschreiber [ausdruck] >> [=[ausdruck]]
```

*Zugehörige Ausgabe:*
```
kulipuptest: 1 TO 10 >> 1 2 3 4 5 6 7 8 9 10
kulipuptest: 1 TO 20 STEP 3 >> 1 4 7 10 13 16 19
kulipuptest: 12 DOWNTO 3 >> 12 11 10 9 8 7 6 5 4 3
kulipuptest: 150 DOWNTO 10 STEP 20 >> 150 130 110 90 70 50 30 10
kulipuptest: 150 DOWNTO 10 STEP 20 TO 14 >> 150 130 110 90 70 50 30 10 11 12 13 14
kulipuptest: 150 DOWNTO 140 TO 145 >> 150 149 148 147 146 145 144 143 142 141 140 141 142 143 144 145
kulipuptest: 1.0 TO 10.0 >> 1.0 2.0 3.0 4.0 5.0 6.0 7.0 8.0 9.0 10.0
kulipuptest: 1.0 TO 10.0 STEP 2 >> 1.0 3.0 5.0 7.0 9.0
kulipuptest: 5.0 TO 10.0 DOWNTO 7.0 >> 5.0 6.0 7.0 8.0 9.0 10.0 9.0 8.0 7.0
kulipuptest: 1.0 TO 11.0 STEP 3.0 DOWNTO 7.0 >> 1.0 4.0 7.0 10.0 9.0 8.0 7.0
```

> Quelle: BSW Puppet Forum >> Bereichsfunktionen TO und DOWNTO / Alternative Benutzung von FOR-Schleifen; sowie Antwort #2
:::



## ISLISTEMPTY

```
ISLISTEMPTY <liste>
```

Rückgabewert ist eine gleichlange Liste wie die untersuchte Liste, bestehend aus Wahrheitswerten `TRUE` / `FALSE`. Für jedes Element wird damit angegeben, ob es leer ist. 


::: details Beispiel
```
UNSET LISTE
ADDLIST LISTE "A" # NON-EMPTY
ADDLIST LISTE "" # EMPTY
ADDLIST LISTE "C X " # NON-EMPTY + NON-EMPTY + EMPTY(!)
ADDLIST LISTE "" # EMPTY
ADDLIST LISTE "Y Z" # NON-EMPTY + NON-EMPTY
>> /tell Kugelschreiber [LISTE] >> [=ISLISTEMPTY [LISTE]]
```

*Zugehörige Ausgabe:*
```
kulipuptest: A C X Y Z >> FALSE TRUE FALSE FALSE TRUE TRUE FALSE FALSE
```
:::



## LISTLENGTH

```
LISTLENGTH <liste>
```

Liefert die Länge der Liste \<liste> (=Anzahl der Elemente).



## FIRSTOF / LASTOF

```
FIRSTOF <liste>
LASTOF <liste>
```

Liefert das erste / letzte Element der Liste \<liste>.



## WITHOUTFIRST / WITHOUTLAST

```
WITHOUTFIRST <liste>
WITHOUTLAST <liste>
```

Liefert alle Elemente der Liste \<liste>, ausser dem ersten / letzten.



## ELEMENTOF

```
<n> ELEMENTOF <liste>
```

Liefert das \<n>-te Element von \<liste>.


```
<list_of_indices> ELEMENTOF <liste>
```

Liefert die angegebenen Element/e von \<liste>.


::: details Beispiel
```
SET LISTE "A B C X Y Z"
SET pos "1"
>> /tell Kugelschreiber [pos] >> [=[pos] ELEMENTOF [LISTE]]
SET pos "2 4 6"
>> /tell Kugelschreiber [pos] >> [=[pos] ELEMENTOF [LISTE]]
SET pos "3 6 9"
>> /tell Kugelschreiber [pos] >> [=[pos] ELEMENTOF [LISTE]]
```

*Zugehörige Ausgabe:*
```
kulipuptest: 1 >> A
kulipuptest: 2 4 6 >> B X Z
kulipuptest: 3 6 9 >> C Z
```
:::



## INLIST

```
<string> INLIST <liste>
```

Liefert `TRUE`, falls \<string> ein Element von \<liste> ist, ansonsten `FALSE`.


```
<liste1> INLIST <liste2>
```

Liefert `TRUE`, falls eines der Elemente aus \<liste1> in \<liste2> enthalten ist, ansonsten `FALSE`.



## ISINDEX

```
ISINDEX <liste>
```

Liefert `TRUE`, falls \<liste> eine Permutation von Indexelementen ist. Das ist dann der Fall, wenn die Liste nur einfach vorkommende Integer-Werte beinhaltet. 

::: details Beispiel
```
EVAL liste = 1 TO 10
>> /tell Kugelschreiber ISINDEX [liste] >> [=ISINDEX [liste]]
EVAL liste = 1, 2, 3 TO 5
>> /tell Kugelschreiber ISINDEX [liste] >> [=ISINDEX [liste]]
EVAL liste = 1, 9999
>> /tell Kugelschreiber ISINDEX [liste] >> [=ISINDEX [liste]]
EVAL liste = 1, 9999 ,9999
>> /tell Kugelschreiber ISINDEX [liste] >> [=ISINDEX [liste]]
EVAL liste = 1.0, 2.0, 3.0 # Ergibt: Ein Fehler ist aufgetreten: java.lang.NumberFormatException: ...
>> /tell Kugelschreiber ISINDEX [liste] >> [=ISINDEX [liste]]
```

*Zugehörige Ausgabe:*
```
kulipuptest: ISINDEX 1 2 3 4 5 6 7 8 9 10 >> TRUE
kulipuptest: ISINDEX 1 2 3 4 5 >> TRUE
kulipuptest: ISINDEX 1 9999 >> TRUE
kulipuptest: ISINDEX 1 9999 9999 >> FALSE
```
:::



## ISFULLINDEX

```
ISFULLINDEX <liste>
```

Liefert `TRUE`, falls \<liste> eine vollständige Permutation von Indexelementen ist. Das ist dann der Fall, wenn die Liste die Integer-Werte 1-N(N=Länge der Liste) in beliebiger Reihenfolge beinhaltet.


::: details Beispiel
```
EVAL liste = 1 TO 10
>> /tell Kugelschreiber ISFULLINDEX [liste] >> [=ISFULLINDEX [liste]]
EVAL liste = 1, 2, 3 TO 5
>> /tell Kugelschreiber ISFULLINDEX [liste] >> [=ISFULLINDEX [liste]]
EVAL liste = 2 TO 9
>> /tell Kugelschreiber ISFULLINDEX [liste] >> [=ISFULLINDEX [liste]]
```

*Zugehörige Ausgabe:*
```
kulipuptest: ISFULLINDEX 1 2 3 4 5 6 7 8 9 10 >> TRUE
kulipuptest: ISFULLINDEX 1 2 3 4 5 >> TRUE
kulipuptest: ISFULLINDEX 2 3 4 5 6 7 8 9 >> FALSE
```
:::



## INVERTINDEX

```
INVERTINDEX <liste>
```

Invertiert eine Permutation (nur für vollständige Permutationen). Liefert also für jedes Indexelement die Stelle, an der dieser Indexwert in der Liste steht. 


::: tip Hinweis
Zweifache Anwendung dieser Funktion auf eine gültige vollständige Permutation liefert wieder den ursprünglichen Index.
:::


::: details Beispiel
```
EVAL liste = "2 4 3 1 5"
>> /tell Kugelschreiber INVERTINDEX [liste] >> [=INVERTINDEX [liste]]
EVAL liste = INVERTINDEX "2 4 3 1 5"
>> /tell Kugelschreiber INVERTINDEX [liste] >> [=INVERTINDEX [liste]]
```

*Zugehörige Ausgabe:*
```
kulipuptest: INVERTINDEX 2 4 3 1 5 >> 4 1 3 2 5
kulipuptest: INVERTINDEX 4 1 3 2 5 >> 2 4 3 1 5
```

> Quelle: BSW Puppet Forum >> Index-Listen/Permutationen / Komma-Operator
:::


## MIN / MAX / SUM / AVG

```
MIN <liste>
MAX <liste>
SUM <liste>
AVG <liste>
```

* Liefert den kleinsten Wert aller Elemente der Liste \<liste>. Leer, falls die Liste leer ist.
* Liefert den größten Wert aller Elemente der Liste \<liste>. Leer, falls die Liste leer ist. 
* Liefert die Summe aller Elemente der Liste \<liste>. Bei `BOOLEAN` ergibt dies die Anzahl der `TRUE`.
* Liefert den Mittelwert aller Elemente der Liste \<liste>. Bei `BOOLEAN` ergibt dies die Häufigkeit.


::: details Beispiel
```
SET LISTE "1 4 -5.6 8"
>> MIN [LISTE] = [=MIN [LISTE]]
>> MAX [LISTE] = [=MAX [LISTE]]
>> SUM [LISTE] = [=SUM [LISTE]]
>> AVG [LISTE] = [=AVG [LISTE]]
#
SET LISTE "TRUE FALSE TRUE"
>> MIN [LISTE] = [=MIN [LISTE]]
>> MAX [LISTE] = [=MAX [LISTE]]
>> SUM [LISTE] = [=SUM [LISTE]]
>> AVG [LISTE] = [=AVG [LISTE]]
```

*Zugehörige Ausgabe:*
```
kulipuptest: MIN 1 4 -5.6 8 = -5.6
kulipuptest: MAX 1 4 -5.6 8 = 8.0
kulipuptest: SUM 1 4 -5.6 8 = 7.4
kulipuptest: AVG 1 4 -5.6 8 = 1.85
kulipuptest: MIN TRUE FALSE TRUE = 0
kulipuptest: MAX TRUE FALSE TRUE = 1
kulipuptest: SUM TRUE FALSE TRUE = 2
kulipuptest: AVG TRUE FALSE TRUE = 0.6666666666666666
```

> Quelle: BSW Puppet Forum >> Bereichsfunktionen TO und DOWNTO / Alternative Benutzung von FOR-Schleifen
:::
