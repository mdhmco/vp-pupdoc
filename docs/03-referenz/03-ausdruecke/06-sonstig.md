# Sonstige Ausdrücke


## ISUSER

```
ISUSER <string>
```

Liefert `TRUE`, wenn User \<string> existiert. `FALSE`, wenn es ein unregistrierter Benutzer ist. 


```
ISUSER <liste>
```

User-Prüfung für alle Elemente der Liste. 


::: tip Hinweis
Verwendung von `MAX ISUSER`:
- 0=Kein Spieler ist registriert
- 1=Mind. ein Spieler ist registriert.

Verwendung von `MIN ISUSER`:
- 0=Mind. ein Spieler ist nicht registriert
- 1=Alle Spieler sind registriert.
:::


::: details Beispiel
```
EVAL liste = "SLC GarantiertUnregNick Kugelschreiber NochEinUnregNick"
>> /tell Kugelschreiber ISUSER [liste] >> [=ISUSER [liste]]
>> /tell Kugelschreiber MAX ISUSER [liste] >> [=MAX ( ISUSER [liste] )]
>> /tell Kugelschreiber MIN ISUSER [liste] >> [=MIN ( ISUSER [liste] )]
```

*Zugehörige Ausgabe:*
```
kulipuptest: ISUSER SLC GarantiertUnregNick Kugelschreiber NochEinUnregNick >> TRUE FALSE TRUE FALSE
kulipuptest: MAX ISUSER SLC GarantiertUnregNick Kugelschreiber NochEinUnregNick >> 1
kulipuptest: MIN ISUSER SLC GarantiertUnregNick Kugelschreiber NochEinUnregNick >> 0
```
:::
