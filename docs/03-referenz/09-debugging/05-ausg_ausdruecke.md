# Ausgabe für Ausdrücke

Weiterhin gibt dieser Befehl auch die Auswertung von Ausdrücken an. Dies geschieht durch::

```
EX <name>: <parameter>
```


::: details Beispiel
Hierbei wird der Ausdruck in Einzelteile zerlegt. Die Auswertung von

```
EVAL x = ( 3 + 5 ) * ( FIRSTOF WITHOUTFIRST "genau 100 Gramm Mehl" )
```

führt zu folgender Debug-Ausgabe

```
C EVAL: >x< >6< (7)
EX +: >3< >5<
EX WITHOUTFIRST: >genau 100 Gramm Mehl<
EX FIRSTOF: >100 Gramm Mehl<
EX *: >8< >100<
```

Dies bedeutet: Der `EVAL`-Befehl hat zwei Parameter, nämlich "x" (die Variable in der das Ergebnis gespeichert werden soll) und "6" (den internen Namen des Ausdrucks, der gleich ausgewertet werden wird).

Dieser Ausdruck berechnet das Produkt von zwei Zahlen (genauer wieder Ausdrücken), nämlich ( 3 + 5 ) und ( `FIRSTOF WITHOUTFIRST` "genau 100 Gramm Mehl" ). Bevor dieses Produkt ausgewertet werden kann, müssen die beiden Faktoren berechnet werden. Die `EX`-Zeilen geben genau die Reihenfolge an, in der die Auswertung stattfindet. 
:::
