# Allgemeines

Enthält das Puppet die Einstellung `DEBUG`, so wird mit dem Start des Puppets ein Debugging-Fenster geöffnet. In diesem Fenster wird unterschiedliche Debugging-Information ausgegeben.

Welche Informationen dies sind, steuert die Liste, die man bei der DEBUG-Einstellung angibt. Diese kann zwischen keinem und allen der folgenden Werte enthalten:

* `ACTION` ➞ Meldet, wenn Aufrufe von Actions ausgelöst werden.
* `EVENT` ➞ Meldet Ereignisse wie Chatmeldungen, auftauchende Spieler, Ablauf des Timer-Events, usw., die ggfs.  Actions auslösen.
* `VARIABLES` ➞ Zeigt Änderungen von Variablen an, allerdings nur für einen im Programm mit `DEBUG ON/OFF` umschlossenen Bereich (wegen der auftretenden  Datenmenge).
* `SINGLESTEP` ➞ Einzelschrittmodus mit Angabe jedes einzelnen Befehls, allerdings wieder nur in bestimmten Bereichen des Programms.
* `LOCAL` ➞ Zeigt Ort und Werte lokaler Variablen (`LOCAL`-Befehl) am Anfang  und Ende der Lebensdauer an.
* `SCOPE` ➞ Jeder Unteraufruf von Actions oder Begin/End-Blöcken wird automatisch durchnummeriert. Hier sieht  man, wie das Programm in solche Blöcke rein- und rausspringt.
* `TOOL` ➞ ...

Die Optionen `SINGLESTEP` und `LOCAL` werden nur aktiv, wenn im Programm irgendwo der Befehl `DEBUG ON` steht. Mit `DEBUG OFF` kann man die Wirkung dieser beiden Werte wieder deaktivieren und damit auf Bereiche des Puppets eingrenzen.


::: warning Warnung
`DEBUG: ""` ist nicht identisch mit `DEBUG: "ON"` (bzw. `DEBUG: ON`). Letzteres aktiviert zusätzlich das Flag für die empfindlichen / ausufernden Ausgaben wie z.B. bei `SINGLESTEP`.
:::
