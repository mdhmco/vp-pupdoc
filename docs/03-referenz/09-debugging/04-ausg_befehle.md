# Ausgabe für Befehle

Gibt jeden einzelnen Befehl aus::

```
C <name>: <parameter> (<Zeilennummer>)
```

Bei den Parametern kann es sein, dass ein `#<zahl>` angegeben wird. Dabei handelt es sich um den (internen) Namen von BEGIN/END-Blöcken, die intern wie eigene Actions behandelt werden. Weiterhin wird bei Ausdrücken nur eine Zahl angegeben. Diese ist ebenfalls ein (interner) Name für den zugehörigen Ausdruck.

Gelegentlich werden weitere Befehle ausgegeben, die nur intern sind, wie etwa `ERROR ON/OFF`, die dazu da sind, dass innerhalb einer Error-Action diese nicht wieder aufgerufen werden kann, etc. 
