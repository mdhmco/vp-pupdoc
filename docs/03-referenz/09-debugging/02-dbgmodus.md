# Debugging-Modus

Der **Starter eines Puppets** kann die Angabe von `DEBUG:<modes>` unter den [Einstellungen für Puppetverhalten](../01-einstell/04-puppetverh#debug) dynamisch ändern: 

* `@@debug ON` ➞ Debugging-Modus anschalten.
* `@@debug OFF` ➞ Debugging-Modus ausschalten.
* `@@debug ACTION` ➞ Bei angeschaltetem Debugging-Modus werden Informationen zur Handhabung von Actions  ausgegeben.
* `@@debug EVENT` ➞ Bei angeschaltetem Debugging-Modus werden Informationen zur Handhabung von Events ausgegeben.
* `@@debug VARIABLES` ➞ Bei angeschaltetem Debugging-Modus werden Informationen zur Handhabung von Variablen ausgegeben.
* `@@debug LOCAL` ➞ Bei angeschaltetem Debugging-Modus werden Informationen zur Handhabung von lokalen Variablen ausgegeben.
* `@@debug SINGLESTEP` ➞ Bei angeschaltetem Debugging-Modus werden Informationen auf der Ebene von Einzelschritten ausgegeben.
* `@@debug SCOPE` ➞ Bei angeschaltetem Debugging-Modus werden Informationen zur Handhabung von Scopes ausgegeben.
* `@@debug TOOL` ➞ Bei angeschaltetem Debugging-Modus werden Informationen zur Handhabung der Tools ausgegeben.
