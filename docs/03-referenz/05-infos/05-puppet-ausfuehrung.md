# Puppet Ausführung


## Puppetstart

```
/puppet start <name>
```

* Start eines **registrierten Puppets**
* \<name> = Name, unter dem das Puppet in der BSW vorher registriert wurde.


```
/puppet CommandPuppet <name> <url>
/puppet CP <name> <url>
```

* Start eines **unregistrierten Puppets**
* \<name> = Name, unter dem das Puppet in der BSW laufen soll.
* \<url> = URL des Puppetcodes, der auf einem eigenen Webspace bereitgestellt wird.



## Serverbelastung

Wenn das Puppet viele Befehle in Folge ausführt, kann dies dazu führen, dass der Server stärker belastet wird. In diesem Fall kann die Anzahl der Befehle, die das Puppet pro Sekunde ausführen kann, von standardmäßig 100 auf bis zu 0 gesenkt werden. Ist der Server zu sehr überlastet (nicht unbedingt nur durch das Puppet), kann es passieren, dass der Server das Puppet ganz rausschmeisst.

Weiterhin überwacht der Server, dass das Puppet nicht zuviel Speicherplatz verbraucht. Zu lange Programme, zu viele Events, zu tief verschachtelte Action-Aufrufe oder zu viele Variablen können dazu führen, dass ein "Overflow" entsteht. Je nach Einstellung beendet sich in diesem Fall das Puppet selber, die Error-Routine wird aufgerufen, oder es passiert einfach gar nix. In den letzten beiden Fällen wird die Variable / der Event etc. einfach nicht gesetzt.

*Maximalbelegung*:

* 10000 = max. Anzahl Sourcezeilen (a' 5000 Zeichen)
* 20000 = max. Anzahl Variablen
* 5000 = max. Länge eines Variablenwertes
* 500 = max. Queue-Länge (=Befehle in der Warteschlange)


::: tip Hinweis
Puppets kann in begründeten Fällen individuell (durch SLC) ein grösserer Maximalwert zugewiesen werden.
:::



## Puppet-Owning

Der OWNER eines Puppets kann durch "/.\<puppetname> \<text>" Texte durch das Puppet ausgeben - bzw. angegebene Kommandos ausführen lassen.


::: tip Hinweis
Dies ist auch für AutoStartPuppets möglich.
:::


Ein Owner muß für die Ausführung von Befehlen ebenfalls (so wie das Puppet) die nötigen Rechte haben. Damit wird verhindert, daß man ein Puppet mit hoher Punktzahl kapern und für Befehle nutzen kann, die man selbst nicht ausführen darf, nur weil ein Puppetschreiber ein `NEWOWNER: NO` im Code vergessen hat.
