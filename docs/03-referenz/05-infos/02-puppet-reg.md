# Puppet-Registrierung

Mit dem Befehl `/puppetdef` können Daten zu einem Puppet unter einem Namen auf dem Server hinterlegt werden. Normalerweise gibt man die Parameter einfach so wie bei `/puppet` an.


## Puppet Registrierung anlegen

```
/puppetdef CommandPuppet <NAME> <URL>
```

* Puppet wird unter dem angegebenen Namen registriert, wobei es beim Start von der ebenfalls angegebenen URL geladen wird.
* Die URL kann sich auch PHP-Skripte referenzieren. Dann wird die URL in der Form `http://server/seite.php?parameter=wert` angegeben.

> Quelle: BSW Puppet Forum >> URL registrierter Puppets wird bei Gleichheitszeichen abgeschnitten



## Puppet Registrierung anzeigen

```
/puppetdef info <NAME>
```



## Puppet Registrierung löschen

```
/puppetdef delete <NAME>
```



## Puppet Registrierung ändern

Beim Unterbefehl `set` können mehrere Werte gleichzeitig gesetzt werden, immer mit `<NAME>=<WERT>` (ohne Leerzeichen). Enthält der Wert Leerzeichen, so müssen Anführungszeichen benutzt werden: `<NAME>="<WERT>"`.


### Typ des Puppets setzen

```
/puppetdef set <NAME> type=<TYPE>
```


### WWW-Adresse für den Quellcode des Puppets setzen

Der Quellcode (das Programm in BSW-Puppetsprache) muss als pure ASCII-Datei auf einem Web-Server hinterlegt werden.

```
/puppetdef set <NAME> url=<URL>
```


### Puppet als "privat" deklarieren

Starten des Puppets ist auf die angegebenen Starter begrenzt.

```
/puppetdef set <NAME> priv (oder "public=Off" )
```


### Puppet als "public" deklarieren

Auch andere Benutzer als die bei "Starter:" angegebenen können dieses Puppet starten, ohne z.B. die URL zu kennen. In diesem Fall werden URL, Kennwort (PWD), Levelpunkte und Stack nicht angezeigt die Karteikarten "debug" und "Statistik" bleiben inaktiv.

```
/puppetdef set <NAME> public (oder "public=On")
```


### Puppet als "public" deklarieren, aber ein Passwort festlegen

Jeder kann das Puppet starten, der das zugehörige Passwort kennt.

```
/puppetdef set <NAME> public pwd=<PWD>
```


### Kurzinformation für die Puppet-Registrierung setzen

```
/puppetdef set <NAME> info="INFO"
```
