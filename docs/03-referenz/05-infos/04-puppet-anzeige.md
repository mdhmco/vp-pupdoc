# Puppet-Anzeigen

## Anzeige Load- und Mem-% in "/who"

Bei `/who` u.ä. sieht der STARTER des Puppets, wie beschäftigt das Puppet ist und wieviel Speicher es verbraucht.


## Anzeigen im Puppettool

### MaxCommand

Dies ist in der Regel ein Wert zwischen 0 und 100 und gibt die Anzahl der in einem (Takt-)Zyklus maximal ausgeführten wesentlichen Operationen wieder. Im Singlestep-Modus (siehe Debugging) wird trotzdem nur 1 Schritt ausgeführt.

*Startwert*: 10


### Abarbeit

Es stehen je nach Belastung des Servers 1/n (n = NICE-Wert, n = 20) der Dauer eines Zyklus (TICKER, normalerweise 1 sek.) zur Verfügung. Das wären bei diesen Werten also 50 ms. Bleibt die Ausführungszeit unterhalb dieser Grenze, schläft das Puppet 950 ms und macht danach mit voller Power weiter. Bei Überschreitung wird die überschüssige Zeit auf Abarbeit draufgeschlagen, das Puppet zur Pause verdammt und der Wert in 50 (ms) Schritten wieder abgebaut. Außerdem wird MaxCommand negativ beeinflußt (z.B. 100 -> 99). Dadurch werden zwar die Überschreitungen möglicherweise reduziert, es führt jedoch bei dauerhafter Überschreitung zu Hemmnissen bis zum Stop des Puppets.

*Startwert*: -


### %load

Bei MaxCommand zwischen 0 und 100 ist %load = 100 - MaxCommand. Startwert ist 90%, es dauert also eine Weile, bis ein Puppet mit voller Power läuft. Zurecht, denn das Laden des Codes und das Parsen belastet den Server nicht unerheblich.

*Startwert*: 90%


### %mem

Prozentualer Anteil des genutzten Speicher am Gesamtspeicher. Als Speicher gelten hier alle möglichen Größen wie z.B. Variablen, Ausdrücke, Eventhandler, Actions, usw.

*Startwert*: -


### Weitere Werte

Darüber hinaus werden noch die folgenden (hier nicht weiter erläuterten) Werte angezeigt:

* Codeblöcke
* Variablen
* Ausdrücke
* Stack
* Speicher
* SleepTo
* NextScope
* GlobalLine
* ACTION
* Kicker

> Quelle: BSW Puppet Forum >> Parameter des Puppet-Tools ? - Antwort #1
