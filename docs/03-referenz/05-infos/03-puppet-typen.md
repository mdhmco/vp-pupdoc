# Puppet-Typen

## SimplePuppets

SimplePuppets kann man i.W. ferngesteuert durch die BSW schicken::

```
/puppet SimplePuppet Marionette
/.Marionette /room C88-19
/.Marionette Huhu, ich bin von SLC geschickt worden
/unown Marionette
/own Marionette
/.Marionette /sound off
/.Marionette /twindow on
/puppet stop Marionette
```

Einige Möglichkeiten bietet auch das PuppetTool, das man sich über rechter Mausklick -> "Configure..." in die Toolleiste holen kann:

* SimplePuppet kann im Befehl `/puppet` mit SP abgekürzt werden.
* SimplePuppets empfangen keine YELLs, da der Owner die YELLs in gewünschter Stufe selbst hören - und das Puppet die Informationen nicht verarbeiten kann.

> Quelle: BSW Puppet Forum >> Simple puppets - Antwort #2



## CommandPuppets

* CommandPuppet kann im Befehl `/puppet` mit CP abgekürzt werden.
* Puppet-Version mit altem (bekannt lauffähigem Code), auf die zurückgefallen werden kann, wenn mal etwas nicht klappt. Soll Dauerausfälle verhindern...



## AutoStartPuppets

* AutoStartPuppets müssen durch SLC als solche freigeschaltet werden (SLC@brettspielwelt.de).
* Nach vollständiger Registrierung (mit URL) und erfolgter Freischaltung einmal mit ``/puppet start <name>`` starten, um das Anstarten nach einem Server-Reboot zu aktivieren.
* AutoStartPuppets existieren zwar ab Serverneustart (auch im PuppetTool) ... werden aber erst rund 5 Minuten später aktiv (geladen).
* Der Start dieser Puppets ist auf 5 Minuten gestreckt (damit über 3 Sekunden pro Puppetstart im Mittel).
* AutoStartPuppets können grundsätzlich nur vom Starter, den übrigen Startern, Admins oder dem Server gekickt werden; kickt jemand anders das Puppet, so begibt es sich in den eigenen Startraum (Ausnahme: Levelpunkte runtersetzen lassen).
* AutoStartPuppetskönnen grundsätzlich nicht gebeamt/gesummont werden (Ausnahme: Levelpunkte runtersetzen lassen).
* AutoStartPuppets können shouten.
* Will man den Autostart-Mechanismus ausstellen, so muß man einmalig das Puppet ohne URL aufrufen, d.h. in der Regel ``/puppet AutoStartPuppet <name>``.


::: tip Tip
Für die Berechtigung zum Autostart schickt man eine Antragsmail an "SLC@brettspielwelt.de". Die Antragsmail soll folgenden Inhalt haben: Username des Starters, Name des Puppets, URL und Begründung, weshalb dies ein AutoStartPuppet werden soll. - Hierzu gibt es auch einen gepinnten Thread im "Puppet" Forum.

*Hinweis*: In 2020 ist diese Aussage vermutlich veraltet.
:::



## Einzelne Puppet Exemplare


### Erklärpuppets

Erklärpuppets werden grundsätzlich so aufgerufen::

```
/puppet TutorPuppet [<spielname>]
/puppet TP [<spielname>]
```

Dabei ist \<spielname> der Name der Gilde des Spiels, muss aber nicht angegeben werden, da ohne Parameter einfach das Spiel im Raum genommen wird.

::: tip Hinweis
Das Puppet kann nicht den Raum wechseln. Wenn alle Spieler den Raum verlassen haben, verschwindet das Puppet automatisch.
:::


### Puppet "Puppetier"

Alternativ kann man ein Puppet aber auch durch ein anderes ASP mitstarten lassen. Dies ist z.B. über das Puppet "Puppetier" möglich.


### Turnierpuppets (von BloodyMary)

BloodyMarys Turnier-Puppet sind auf dem BSW Server hinterlegt. Um diese zu nutzen wird eine symbolische URL genutzt.

```
/puppet CommandPuppet MeineLiga <turnier>
/puppetdef CommandPuppet MeineLiga <turnier>
/puppetdef set MeineLiga url=<turnier>
```


Als Angabe \<turnier> können folgende Optionen angegeben werden:

- `turnier/3` - Version 3 der Puppets
- `turnier/4` - Version 4 der Puppets
- `turnier/4beta` - Version 4 (beta) der Puppets

> Quelle: BSW Puppet Forum >> Turnierpuppets starten nicht - Antwort #6
