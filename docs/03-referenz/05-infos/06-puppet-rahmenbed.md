# Puppet Rahmenbedingungen


## Beachtenswertes für Puppetautoren

* Thread zu den Rahmenbedingungen der Puppetausführung in der BSW im [Puppet Forum](http://www.brettspielwelt.de/Forum/index.php?topic=153390.0).
* Siehe auch Erwähnung von Puppets in den [BSW Datenschutzbestimmungen](https://intro.brettspielwelt.de/datenschutz.html) - Punkt 1.3.
