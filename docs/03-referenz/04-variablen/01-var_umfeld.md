# Variablen des Umfelds

## CHARSET

- Zeichensatz des Puppetsource (Beispiel: ISO-8859-1).


## __FILE
- Name der Puppetsource (ohne Dateiendung).


## __GAME\<#M>\<S>

- Rückgabe der Anzahl Spieltaler des angebenen Spiels `S` bei der ebenfalls angegebenen Anzahl an Mitspielern `#M`
- Beispiel: `[__GAME4CantStop] = 7`
- Wird als `#M` eine ungültige Mitspieler-Anzahl übergeben (z.B. "0"), so wird die Spiel-ID des Spiels (CantStop 3, Crosswise 115, ...) zurückgegeben.


## __MAXPUBLIC

- Anzahl der dem Puppet zur Verfügung stehenden PUBLIC-Variablen.
- Diese Anzahl kann durch SLC erhöht werden.
- *Default*: 100


## __MAXSAVE

- Anzahl der dem Puppet zur Verfügung stehenden SAVE-Variablen.
- Diese Anzahl kann durch SLC erhöht werden.
- *Default*: 100


## __VERSION

- Versions-Nr. von ??? (Beispiel: "$Revision: 1.126 $").


> Quelle: BSW Puppet Forum >> GURU - Best Practices der Puppet-Programmierung - Antwort #18
