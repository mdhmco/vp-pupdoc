# Variablen von Befehlen

## Variable WHO

* Stammt eine Ausgabe vom Server ist `[WHO]` mit `--` belegt.
* Stammt der Text von einem //-Befehl so steht `//` drin.
* Ansonsten ist der Name des Schreibers enthalten.


## Variable TYPE

Diese Variablen gibt die Art des Chats an:

* `CHAT` ➞ Eingabe erfolgt im Haupt-Chat-Fenster
* `TELL` ➞ Eingabe erfolgte in einem Tell-Fenster zum Puppet
* `CTELL` ➞ Eingabe erfolgte im Stadt-Chat-Fenster
* `GTELL` ➞ Eingabe erfolgte in einem Channel-Fenster


## Variable ROOM

Diese Variable gibt den Aufenthaltsort des Sprechers - in Abhängigkeit der Variablen `TYPE` - an:

* Type `CHAT` ➞ Name des Raums, in dem sich der Sprecher befindet
* Type `TELL` ➞ Name des Raums, in dem sich der Sprecher befindet
* Type `CTELL` ➞ Name der Stadt
* Type `GTELL` ➞ Name des Kanals


## Variable ROOMNAME

Diese Variable gibt eine Angabe zum Aufenthaltsort des Sprechers - in Abhängigkeit der Variablen `TYPE` - an:

* Type `CHAT` ➞ Name der Stadt
* Type `TELL` ➞ Name der Stadt
* Type `CTELL` ➞ Name des Kanals (#\<Stadtname>)
* Type `GTELL` ➞ #Name des Kanals (#\<Channelname>)
