# Einstellungen für Puppetowning

## OWN

- Stellt ein, ob man das Puppet nach dem Start vom Starter geowned wird.
- *Default*: `YES`


## NEWOWNER

- Legt fest, ob das Puppet von anderen Benutzern geowned werden darf.
- Wird automatisch auf `NO` gestellt, wenn `CITYCHAT:YES` gesetzt ist.
- *Default*: `YES`


## TWINDOW

- Das, was ein Puppet zu hören bekommt, wird nach `/own PUPPET` durch `TWINDOW:YES` in einem extra Fenster dargestellt. Ansonsten erscheint dies im normalen Chat.
- *Default*: `NO`
