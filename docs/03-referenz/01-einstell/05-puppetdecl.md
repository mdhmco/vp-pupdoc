# Einstellungen für Puppetdeklaration

## CHARSET

- Der Zeichensatz des Quelltextes wird festgelegt.
  - `ISO-8859-1`
  - `UTF-8`
  - `Shift_JIS`
- *Default*: `ISO-8859-1`
