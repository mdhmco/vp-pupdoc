# Einstellungen für Puppetverhalten

## CASESENSITIV

- Gibt an, ob das Puppet Groß-/Kleinschreibung bei den Events `CHAT`, `KEYWORD` und `MATCH` unterscheidet.
- Die Einstellung `CASESENSITIV:NO` wird auch bei `==, !=, IN` (nur `EVAL`!), `STARTSWITH`, `ENDSWITH` und `INLIST` beachtet.
- *Default*: `NO`


## OVERFLOW

- Es wird festgelegt, was geschehen soll, wenn das Puppet zu viele
 Ressourcen (Speicher, ..) verbraucht:
  - `ERROR` = Es wird eine ERROR-Action aufgerufen (:gear: siehe Kommando [ERROR-Ereignis &raquo; WHEN ERROR](../02-befehle/04-ereignisse/11-error)).
  - `HARAKIRI` = Das Puppet beendet sich.
  - `IGNORE` = Das Puppet ignoriert den Fehler.
- *Default*: `HARAKIRI`


## ZEROINTVARS

- Wenn `YES` angegeben wird, werden leere Variablen als "0" aufgefasst.
- *Default*: `NO`

::: warning Warnung
Es gilt [undefined] >= 0 und [undefined] <= 0 aber nicht [undefined] == 0.
:::


## DEBUG

- Hiermit wird der DEBUG-Modus angeschaltet. Erlaubt ist jede Kombination der Einstellungen: `ACTION`, `EVENT`, `VAR(IABLES)`, `(SINGLE)STEP`, `LOCAL`, `SCOPE` und `TOOL`
- Ausserdem `ON`, `OFF`, `ALL` und `NONE`.
- *Default*: keiner

:gear: Siehe PuppetTool und Referenz [Debugging](../09-debugging/00-page)


### @\<command>
- Aufruf der festgelegten Action als @-Befehl.
- *Default*: keiner

:gear: Siehe Kapitel [@-Befehle](../../02-basic/04-addbefehle#addbefehle)
