# Einstellungen für Puppettexte

## LOGIN

- Diese Einstellung legt den Text fest, der bei der Anmeldung ausgegeben wird. Leerzeichen am Anfang nicht vergessen.
- *Default*: " materialisiert sich aus milliarden winziger Teilchen."


## LOGOUT

- Diese Einstellung legt den Text fest, der bei der Abmeldung ausgegeben wird. Leerzeichen am Anfang nicht vergessen.
- *Default*: " zerfaellt zu Staub."


## APPEAR

- Diese Einstellung legt den Text fest, der beim Betreten eines Raumes ausgegeben wird. Leerzeichen am Anfang nicht vergessen.
- *Default*: " verdichtet sich aus einer Wolke glitzernder Partikel."


## DISAPPEAR

- Diese Einstellung legt den Text fest, der beim Verlassen eines Raumes ausgegeben wird. Leerzeichen am Anfang nicht vergessen.
- *Default*: " zerspringt in millionen funkelnder Partikel."


## INFO

- Es wird die Beschreibung des Puppets festgelegt, die beim `\info`-Kommando angezeigt wird.
- *Default*: "Ein CommandPuppet."
