# Einstellungen für Puppetcode

## NATION

- Entspricht in der BrettspielWelt.prop in etwa `NATION = <lang>` oder einem frühzeitigen `>> /language <lang>` z.B. in der ACTION "start".
- *Default*: `bsw`


::: tip Tip
Die Sprache "bsw" ist zu empfehlen, da diese nicht durch unbedarfte Übersetzer kaputt gemacht werden kann.
:::


## CITYCHAT

- Legt fest, ob das Puppet den Citychat des Starters benutzt. Bei Autostart-Puppets gilt derjenige als Starter, der das Puppet letztmalig manuell gestartet hat.
- `CITYCHAT:YES` bewirkt automatisch `NEWOWNER:NO`.
- Wird ein Puppet von einem anderen Puppet gestartet, so kann es Probleme mit dem Stadt-Chat geben
- *Default*: `NO`


## SAVE

- Der SAVE-Befehl macht die SAVE-Variablen persistent, d.h. wenn das Puppet wieder geladen wird, sind diese Variablen mit den gespeicherten Werte belegt.

  - Die Angabe "phrase" sollte weltweit eindeutig sein (Sonderzeichen werden automatisch durch "_" ersetzt).
  - "*" in "phrase" wird durch den Namen ersetzt, mit dem das Puppet in der BSW gestartet wird (dies erhöht die Chance auf Eindeutigkeit).
  - Bei gleichem Namen wird dieselbe Datenbasis benutzt. Dies kann in seltenen Fällen Sinn machen (allerdings sollten zwei Puppets mit denselben Daten nicht gleichzeitig gestartet sein).
- *Default*: keiner


::: warning Warnung
Wird `SAVE:` nicht angegeben wird, dann werden keine SAVE-Variablen gespeichert.
:::
