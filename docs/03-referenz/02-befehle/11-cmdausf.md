# Befehle für Kommandoausführung

## BSW-Kommandos

Puppets können einige BSW-Kommandos ausführen. Hierzu muss das Kommando incl. des Schrägstrichs (/) mittels der offenen Textausgabe ausgegeben werden.


*Liste der für Puppets erlaubten BSW-Kommandos*:

* `/blacklist`
* `/channel`
* `/cityinfo`
* `/ctell` (Voraussetzung: `CITYCHAT: YES`)
* `/emotion`
* `/game` (nicht erlaubt bei AutoStartPuppets)
* `/ghook` (nicht erlaubt bei AutoStartPuppets)
* `/gtell`
* `/hook` (nicht erlaubt bei AutoStartPuppets)
* `/inhabitants`
* `/language`
* `/leave`
* `/msg`
* `/mtell`
* `/mutectell`
* `/name`
* `/puppet start`
* `/puppet stop`
* `/reset`
* `/room` (nicht erlaubt bei AutoStartPuppets)
* `/shout` (Voraussetzung: `CITYCHAT: YES`)
* `/tell`
* `/who`


::: tip Hinweis
Wird versucht, ein anderes Kommando auszuführen, so wird dieses ignoriert und stattdessen als Text im Hauptchat ausgegeben.
:::



## Puppetstart

```
>> /puppet start <ANDERESPUPPET>
```

* Mittels dieses Befehls kann von einem Puppet ein weiteres Puppet gestartet werden.
* Dies ist an folgende Bedingungen geknüpft:
  - Das zu startende Puppet muss registriert sein - und
  - Das startende Puppet muss mindestens 2800 Levelpunkte besitzen (diese Zahl soll lt. SLC vom Puppettyp abhängig sein).


::: tip Hinweis
Die Lade- und "Kompilier"-Zeit des gestarteten Puppets geht zu Lasten des startenden Puppets. Ist diese lang (schlechte Verbindung, viel Code, andere Faktoren), so gibt es hohe Abarbeit-Werte und das Puppet legt eine entsprechend lange Pause ein.
:::

> Quelle: BSW Puppet Forum >> Unterschiede bei "Puppet startet Puppet" - Antwort #1


### Levelpunkte

Standardmässig besitzt ein Puppet 2000 Levelpunkte. Jeder hiervon abweichende Wert wurde seitens SLC manuell vergeben.

Ein Puppet mit 120.000 Levelpunkten ist 1 Punkt über den x20/x21-Usern. Damit ist ein Puppet mit diesen Levelpunkten ranghöchste Person (mit Ausnahme der Administratoren und dem Aldermann) innerhalb eines Channels. 



## Puppetstop

```
>> /puppet stop <ANDERESPUPPET>
```

* Mittels dieses Befehls kann von einem Puppet ein weiteres Puppet gestoppt werden.
* Dies geht nur bei Puppets, die zuvor gestartet wurden. 
