# Befehle für Strings


## FIND-Befehl

```
FIND <varname> <zeichen>
```

* Sucht in der Variablen mit Namen \<varname> alle Zeichen \<zeichen>.


::: details Liste der belegten Variablen
* `INDEXLEN` = Anzahl der Treffer
* `INDEX*` = Positionen der Treffer (\*=1,2,...)
:::



## REPLACE-Befehl

```
REPLACE <varname> <von> <nach>
```

* Ersetzt in der Variablen mit Namen \<varname> alle Vorkommen \<von> durch \<nach> (ohne Rücksicht auf Listenformate).



## SUBSTR-Befehl

```
SUBSTR <varname> <indexVon> <indexBis>
```

* Ersetzt den String mit Namen \<varname> durch eine Teilmenge seiner selbst.
* Die Teilmenge wird bestimmt durch die Zeichen zwischen den Indexpositionen \<indexVon> und \<indexBis>.
* Wird \<indexBis> weggelassen, so werden die Zeichen bis zum Ende genommen.


::: tip Tip
Für \<indexVon> gleich \<indexBis> empfiehlt sich der Ausdruck [CHAROF](../03-ausdruecke/04-strings#charof).
:::
