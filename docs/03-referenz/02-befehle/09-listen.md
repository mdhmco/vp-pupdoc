# Befehle für Listen


## ADDLIST-Befehl

```
ADDLIST <listname> <element1> <element2> ...
```

* Hängt weitere Element/e hinten an die Liste mit Namen \<listname> an.


::: tip Tip
Dieser Befehl ist der üblichen Schreibweise `SET listname "[listname] [element1] [element2]..."` vorzuziehen, wenn man Probleme aufgrund überflüssiger Leerzeichen o.ä. vermeiden will.
:::


::: tip Hinweis
Ein besonderes Augenmerk verdient außerdem die *1-elementige Liste* mit einem leeren Eintrag "", die nur bei Anwendung der Listenbefehle von der *leeren Liste* ("") unterschieden werden kann.
:::



## INSERTLIST-Befehl

```
INSERTLIST <listname> <index> <element1> <element2> ...
```

* Fügt weitere Element/e so in die Liste mit Namen \<listname> ein, dass sich \<element1> später an der Indexposition \<index> befindet.


::: tip Tip
Ist \<index> gleich der (Listenlänge plus 1), dann entspricht dies dem [ADDLIST-Befehl](../02-befehle/09-listen#addlist-befehl).
:::



## SETELEMENT-Befehl

```
SETELEMENT <listname> <index> <element>
```

* Setzt in der Liste mit dem Namen \<listname> das Element an Position \<index> auf den Wert von \<element>.


::: tip Tip
Ist \<index> gleich der (Listenlänge plus 1), dann entspricht dies dem [ADDLIST-Befehl](../02-befehle/09-listen#addlist-befehl) mit einem Element.
:::



## REPLACELIST-Befehl

```
REPLACELIST <listname> <elementListe> <element>
```

* Ersetzt in der Liste mit dem Namen \<listname> alle Elemente, die identisch mit einem der Elemente in der Liste \<elementListe> sind, durch das Element \<element>.
* Das Listenformat wird hierbei berücksichtigt.



## REMOVEELEMENT-Befehl

```
REMOVEELEMENT <listname> <index>
```

* Löscht in der Liste mit Namen \<listname> das Element an der Indexposition \<index>.
* Der \<index> darf dabei die Werte (1 bis Listenlänge) annehmen.



## REMOVELIST-Befehl

```
REMOVELIST <listname> <element1> <element2> ...
```

* Löscht in der Liste mit Namen \<listname> alle Elemente, die identisch den angegebenen Element/en sind.



## SUBLIST-Befehl

```
SUBLIST <listname> <indexVon> [<indexBis>]
```

* Ersetzt die Liste mit Namen  \<listname> durch eine Teilmenge ihrer selbst.
* Die Teilmenge wird bestimmt durch die Indexpositionen \<indexVon> und \<indexBis>.
* Wird \<indexBis> weggelassen, so werden alle Elemente bis zum Ende genommen.


::: tip Tip
Für \<indexVon> gleich \<indexBis> empfiehlt sich der Ausdruck [ELEMENTOF](../03-ausdruecke/05-listen#elementof).
:::


::: details Beispiel
```
LOCAL Liste = "SLC Stechmuck Kugelschreiber"
SUBLIST Liste 2          # identisch mit SUBLIST Liste 2 3
>> [Liste]               # liefert "Stechmuck Kugelschreiber"
```
:::



## FINDLIST-Befehl

```
FINDLIST <listname> <element>
```

* Sucht in der Liste mit Namen \<listname> alle Elemente mit dem Inhalt von \<element>.


::: details Liste der belegten Variablen
* `INDEXLEN` = Anzahl der Treffer
* `INDEX*` = Positionen der Treffer (\*=1,2,...)
:::


::: details Beispiel
```
# --- Spiel suchen
FINDLIST _LISTE_SPIELE_ [resGame]
#IF [INDEXLEN] != 1 # (alternative Abfrage, wenn Spiele max. 1x in Liste enthalten)
IF [INDEXLEN] == 0 # (allgemeinere Abfrage)
BEGIN
  >> /gtell [_channel_] INFO: Spiel ([resGame]) nicht in Liste gefunden ([_LISTE_SPIELE_])
  RETURN
END
```
:::



## SORTLIST-Befehl

```
SORTLIST <listname>
```

* Sortiert die Elemente der Liste mit Namen \<listname> alphabetisch aufsteigend.
* Sollen Ziffern sortiert werden, so müssen die Elemente der Liste zuvor besonders aufbereitet worden sein (Beispiel: Auffüllung mit führenden Nullen bis zu einer einkeitlichen Länge von x Stellen; Achtung bei negativen Zahlen).


::: details Liste der belegten Variablen
* `INDEX` = Nach der Sortierung beinhaltet die Liste mit Namen "INDEX" die Positionen der (ursprünglichen) Elemente vor der Sortierung. 
:::


::: details Beispiel
```
ACTION eineAction
  LOCAL liste = "gustav1 doris2 franz3 heinrich4 manuela5 anton6 conny7 emil8 bernd9 ingrid10"
  >> /tell [WHO] OriginalListe: [liste]
  SORTLIST liste
  >> /tell [WHO] Sortierte Liste: [liste]
  >> /tell [WHO] >> INDEX: [INDEX]
  SHUFFLELIST liste
  >> /tell [WHO] Geschüttelte Liste: [liste]
  >> /tell [WHO] >> INDEX: [INDEX]
END
```

*Zugehörige Ausgabe:*
```
OriginalListe: gustav1 doris2 franz3 heinrich4 manuela5 anton6 conny7 emil8 bernd9 ingrid10
Sortierte Liste: anton6 bernd9 conny7 doris2 emil8 franz3 gustav1 heinrich4 ingrid10 manuela5
>> INDEX: 6 9 7 2 8 3 1 4 10 5
Geschüttelte Liste: anton6 conny7 emil8 ingrid10 manuela5 doris2 gustav1 bernd9 franz3 heinrich4
>> INDEX: 1 3 5 9 10 4 7 2 6 8
```
:::



## REVERTLIST-Befehl
-----------------

```
REVERTLIST <listname>
```

* Dreht die Liste mit Namen \<listname> um.


::: tip Hinweis
Damit ist zusammen mit dem [SORTLIST-Befehl](#sortlist-befehl) auch ein *absteigendes Sortieren* möglich.
:::


::: details Beispiel für absteigendes Sortieren
```
ACTION eineAction
  LOCAL liste = "gustav1 doris2 franz3 heinrich4 manuela5 anton6 conny7 emil8 bernd9 ingrid10"
  SORTLIST liste
  REVERTLIST liste
END
```
:::



## SHUFFLELIST-Befehl

```
SHUFFLELIST <listname>
```

* Mischt die Elemente der Liste  \<listname> zufällig. 


::: details Liste der belegten Variablen
* `INDEX` = Nach dem Mischen beinhaltet diese Liste mit Namen "INDEX" die Positionen der (ursprünglichen) Elemente vor dem Mischen.
:::


::: details Beispiel
```
ACTION eineAction
  LOCAL liste = "gustav1 doris2 franz3 heinrich4 manuela5 anton6 conny7 emil8 bernd9 ingrid10"
  >> /tell [WHO] OriginalListe: [liste]
  SORTLIST liste
  >> /tell [WHO] Sortierte Liste: [liste]
  >> /tell [WHO] >> INDEX: [INDEX]
  SHUFFLELIST liste
  >> /tell [WHO] Geschüttelte Liste: [liste]
  >> /tell [WHO] >> INDEX: [INDEX]
END
```

*Zugehörige Ausgabe*:
```
OriginalListe: gustav1 doris2 franz3 heinrich4 manuela5 anton6 conny7 emil8 bernd9 ingrid10
Sortierte Liste: anton6 bernd9 conny7 doris2 emil8 franz3 gustav1 heinrich4 ingrid10 manuela5
>> INDEX: 6 9 7 2 8 3 1 4 10 5
Geschüttelte Liste: anton6 conny7 emil8 ingrid10 manuela5 doris2 gustav1 bernd9 franz3 heinrich4
>> INDEX: 1 3 5 9 10 4 7 2 6 8
```
:::
