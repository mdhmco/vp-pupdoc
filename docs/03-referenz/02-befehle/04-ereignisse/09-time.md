# TIME-Ereignis

## WHEN TIME

```
WHEN TIME <hour>[:<min>] DO <action>
```

- Jedes Mal zu der (Server-)Uhrzeit, die von \<hour> und \<min> bestimmt wird, wird die Action \<action> ausgeführt. 


::: warning Warnung
Das Puppet kann sich nur eine Uhrzeit merken.
:::



## IGNORE TIME

```
IGNORE TIME
```

- Puppet führt nichts mehr zu einer bestimmten Uhrzeit aus.
