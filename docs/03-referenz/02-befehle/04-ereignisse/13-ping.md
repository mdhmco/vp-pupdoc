# PING-Ereignis

## WHEN PING

```
WHEN PING [FROM <liste>] DO <action>
```

- Wenn jemand aus \<liste> das Puppet anpingt, wird die Action  \<action> ausgeführt.


::: details Liste der belegten Variablen
* `WHO` = Person (bzw. Puppet), die den Ping ausgelöst hat
:::



## IGNORE PING

```
IGNORE PING [FROM <liste>]
```

- Wenn jemand aus \<liste> das Puppet anpingt, wird nichts mehr gemacht.
- Wird \<liste> ganz weggelassen, reagiert das Puppet auf überhaupt keine Pings mehr.
