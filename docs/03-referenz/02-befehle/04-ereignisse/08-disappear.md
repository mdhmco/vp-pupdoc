# DISAPPEAR-Ereignis

## WHEN APPEAR

```
WHEN DISAPPEAR <liste> DO <action>
```

- Wenn jemand aus \<liste> den Raum verlässt wird die Action \<action> ausgeführt.
- Zusätzlich geschieht dies durch die Eigenbewegung des Puppets. 


::: details Liste der belegten Variablen
* `WHO` = Person, die den Raum verlässt
:::


::: tip Hinweis
Personen, deren Name mit "Geist" beginnt, werden ignoriert (=Leute, die connected haben, aber noch nicht eingeloggt sind).
:::



## IGNORE DISAPPEAR

```
IGNORE DISAPPEAR <liste>
```

- In Zukunft wird nichts gemacht, wenn jemand aus \<liste> den Raum verlässt.
- Genauer ausgedrückt: Es wird ein vorhergehendes `WHEN DISAPPEAR <liste> DO xxx`  gelöscht.



## IGNOREALL DISAPPEAR

```
IGNOREALL DISAPPEAR
```

- Alle DISAPPEAR-Einträge werden gelöscht (default).
