# MATCH-Ereignis

## WHEN MATCH

```
WHEN MATCH <match> [erweiterte WHEN Bedingungen] DO <action>
```

- Wenn das Puppet Eingabezeilen mitbekommt, auf die \<match> passt, so wird Action \<action> ausgeführt.
- Die Wirkungsweise des Befehls kann durch [Erweiterte WHEN Bedingungen](./01-info-erwbed#erweiterte_when_bedingungen) eingeschränkt werden.


::: details Liste der belegten Variablen
* `WHO` = Sprecher des Textes
* `CHAT` = Eingegebener Text
* `SUBST1` = Substitution für erstes * in ``MATCH``
* `SUBST2` = Substitution für zweites * in ``MATCH``
* `SUBSTx` = Substitution für x-tes * in ``MATCH``
* `SUBSTLEN` = Anzahl der Substitutionen 
* `TYPE` = Art des Chats (:gear: Siehe [Variable TYPE](../04-other/01-variablen#variable-type))
* `ROOM` = Aufenthaltsort des Sprechers (:gear: Siehe [Variable ROOM](../04-other/01-variablen#variable-room))
* `ROOMNAME` = Angabe zum Aufenthaltsort des Sprechers (:gear: Siehe [Variable ROOMNAME](../04-other/01-variablen#variable-roomname))
:::



## IGNORE  MATCH

```
IGNORE MATCH <match> [erweiterte WHEN Bedingungen]
```

- Der \<match> wird ab sofort nicht mehr überprüft bzw. nicht mehr
  gesondert behandelt, falls noch eine "*"-Eintragung existiert.
- Die Wirkungsweise des Befehls kann durch [Erweiterte WHEN Bedingungen](./01-info-erwbed#erweiterte_when_bedingungen) eingeschränkt werden.



## IGNOREALL MATCH

```
IGNOREALL MATCH [erweiterte WHEN Bedingungen]
```

- Der \<match> wird ab sofort nicht mehr überprüft bzw. nicht mehr gesondert behandelt, falls noch eine "*"-Eintragung existiert.
- Die Wirkungsweise des Befehls kann durch [Erweiterte WHEN Bedingungen](./01-info-erwbed#erweiterte_when_bedingungen) eingeschränkt werden.
