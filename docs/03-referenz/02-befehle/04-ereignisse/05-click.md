# CLICKED-Ereignis

## WHEN CLICKED

```
WHEN CLICKED [erweiterte WHEN Bedingungen] DO <action>
```

- ...TO BE SPECIFIED...


::: details Liste der belegten Variablen
* `WHO` = Sprecher des Textes
* `CHAT` = Eingegebener Text
* `ITEM` = ???
* `TYPE` = Art des Chats (:gear: Siehe [Variable TYPE](../04-other/01-variablen#variable-type))
* `ROOM` = Aufenthaltsort des Sprechers (:gear: Siehe [Variable ROOM](../04-other/01-variablen#variable-room))
* `ROOMNAME` = Angabe zum Aufenthaltsort des Sprechers (:gear: Siehe [Variable ROOMNAME](../04-other/01-variablen#variable-roomname))
:::



## IGNORE CLICKED

```
IGNORE CLICKED [erweiterte WHEN Bedingungen]
```

- ...TO BE SPECIFIED...
