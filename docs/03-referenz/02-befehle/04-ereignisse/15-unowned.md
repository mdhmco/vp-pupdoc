





# UNOWNED-Ereignis

## WHEN UNOWNED

```
WHEN UNOWNED DO <action>
```

- Wenn das Puppet freigesetzt wird, wird die Action \<action> ausgeführt.



## IGNORE UNOWNED

```
IGNORE UNOWNED
```

- Beim Freisetzen des Puppets passiert nichts.
