# APPEAR-Ereignis

## WHEN APPEAR

```
WHEN APPEAR <liste> DO <action>
```

- Wenn jemand aus \<liste> den Raum betritt wird die Action \<action> ausgeführt.
- Zusätzlich geschieht dies durch die Eigenbewegung des Puppets.


::: details Liste der belegten Variablen
* `WHO` = Person, die den Raum betritt 
:::


::: tip Hinweis
Personen, deren Name mit "Geist" beginnt, werden ignoriert (=Leute, die connected haben, aber noch nicht eingeloggt sind).
:::



## IGNORE APPEAR

```
IGNORE APPEAR <liste>
```

- In Zukunft wird nichts gemacht, wenn jemand aus \<liste> den Raum betritt.
- Genauer ausgedrückt: Es wird ein vorhergehendes `WHEN APPEAR <liste> DO xxx` gelöscht.
- Die Wirkungsweise des Befehls kann durch [Erweiterte WHEN Bedingungen](./01-info-erwbed#erweiterte_when_bedingungen) eingeschränkt werden.

> Quelle: BSW Puppet Forum >> Ignore Appear - Antwort #1



## IGNOREALL APPEAR

```
IGNOREALL APPEAR
```

- Alle APPEAR-Einträge werden gelöscht (default).
- Die Wirkungsweise des Befehls kann durch [Erweiterte WHEN Bedingungen](./01-info-erwbed#erweiterte_when_bedingungen) eingeschränkt werden.
