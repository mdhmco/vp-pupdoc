# ERROR-Ereignis

## WHEN ERROR

```
WHEN ERROR DO <action>
```

- Tritt ein Fehler bei der Ausführung eines Puppets auf (Teilen durch 0, etc.)  so wird die Action \<action> ausgeführt (und zwar sofort und nicht erst nachdem die aktuelle Action beendet wurde). 


::: tip Hinweis
Tritt während der Ausführung der Action \<action> ein Fehler auf, so wird diese nicht erneut ausgeführt. 
:::


::: details Liste der belegten Variablen
* `MESSAGE` = Fehlermeldung (in der Regel eine Java-Fehlermeldung)
:::



## IGNORE ERROR

```
IGNORE ERROR
```

- Tritt ein Fehler auf, so wird nichts gemacht.
