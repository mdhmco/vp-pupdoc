# NEWROOM-Ereignis

## WHEN NEWROOM

```
WHEN NEWROOM DO <action>
```

- Wenn das Puppet den Raum wechselt, wird die Action \<action>  ausgeführt.


::: details Liste der belegten Variablen
* `ROOM` = ID des neuen Raumes
:::



## IGNORE NEWROOM

```
IGNORE NEWROOM
```

- Raumwechsel werden nicht mehr beachtet.
