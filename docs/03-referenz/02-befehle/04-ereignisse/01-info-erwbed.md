# Infos zu Befehlen für Ereignisse

## Erweiterte WHEN Bedingungen

Bei den Befehlen `WHEN CHAT`, `WHEN KEYWORD`, `WHEN MATCH` und `WHEN CLICKED` kann die Wirkung des Events anhand weiterer Kriterien eingeschränkt werden. Hierbei sind folgende Möglichkeiten gegeben: 

```
WHEN ... FROM <who> DO ...
WHEN ... [FROM <who>] IN <type> <room> DO ...
WHEN ... [FROM <who>] TYPE <type> DO ...
WHEN ... [FROM <who>] ROOM <room> DO ...
WHEN ... [FROM <who>] TYPE <type> ROOM <room> DO ...
```

* \<who> = Einschränkung auf `who`
  - Hier kann eine Liste von Personen angegeben werden.
  - Wird in der Liste ein "*" angegeben, so wird ein evtl. existierender konkreter Eintrag zusätzlich (also doppelt) ausgeführt.


::: tip Tip
Das Puppet reagiert auf sich selbst nur dann, wenn es explizit in der Liste der Personen aufgeführt ist, da es vom Eintrag "*" ausgeschlossen ist.
:::


* \<type> = Einschränkung auf `TYPE`
  - `CHAT` = Eingabe erfolgt im Haupt-Chat-Fenster
  - `TELL` = Eingabe erfolgt in einem Tell-Fenster zum Puppet
  - `CTELL` = Eingabe erfolgt im Stadt-Chat-Fenster
  - `GTELL` = Eingabe erfolgt in einem Channel-Fenster
  - `YELL1` = Eingabe erfolgt über Yell der Stufe [1]
  - `YELL2` bis `YELL6` = entsprechend Stufe [2]-[6]
  - `YELL7` = Eingabe erfolgt über Yell der Stufe [7] (Admin-/Aldiyell) 
* \<room> = Einschränkung auf `ROOM`, also Raum/Kanal

> Quelle: BSW Puppet Forum >> Yells ignorieren - Antwort #4 und >> tell um tell, shout um shout - Antwort #8


::: details An anderer Stelle beschreibt es SLC wie folgt
```
  WHEN ... [FROM <userlist>] { [TYPE <typelist>] [ROOM <roomlist>] | [IN <typelist> <roomlist>] }
```

Where \<userlist>, \<typelist> and \<roomlist> may contain the values "*" or names (or not being specified). You can specify multiple names (as a list, that means, that you have to use " around), but be aware of the fact, that each combination creates it's own trigger.

> Quelle: BSW Puppet Forum >> Puppet Dokumentation vom Kugelschreiber - Antwort #67 und >> NOT FROM in WHEN-Abfrage - Antwort #3
:::


::: tip Hinweis
Wurden Variablen bei den einzelnen Schlüsselworten angegeben, so werden diese nur zum Zeitpunkt der Ausführung des `WHEN` Befehls ausgewertet. Eine nachträgliche Änderung des Variableninhaltes zeigt hier keine Wirkung.
:::



## Grundprinzip der erweiterten WHEN Bedingungen

Die Einschränkung erfolgt 3-stufig mittels nachfolgender Kriterien, die in der Reihenfolge `WHO`, `TYPE`, `ROOM` ausgeführt werden. Diese Reihenfolge wird immer eingehalten, auch wenn man im Quellcode diese Angaben umstellen darf. 

- Wird eine Stufe weggelassen, so wird das Statement bei Relevanz der übrigen Stufen ausgeführt.
- Wird für eine Stufe "*" angegeben so wird es ausgeführt, wenn es kein Statement für den konkreten `WHO/TYPE/ROOM` gibt.
- Ist ein Statement mit dem konkreten `WHO/TYPE/ROOM` vorhanden, so wird ein Eintrag mit einem "*" nicht zusätzlich ausgeführt.

Da dieses Grundprinzip für jede Stufe gilt, ergeben sich hieraus
3-hoch-3 (=27) Fälle.


::: details Beispiel
```
ACTION eineAction
  # Registrierung diverser Actions
  #  - Reaktion auf Schlüsselwort(e) aus einem bestimmten Raum
  WHEN KEYWORD "someKeyword" FROM "*" IN ROOM "C125-41" DO someAction
  #  - Reaktion auf Schlüsselwort(e) aus bestimmten Quellen
  WHEN KEYWORD "someKeyword" TYPE "CTELL TELL GTELL" DO someAction
  #  - Reaktion auf Textausgaben von Personen, die in einer Liste stehen
  WHEN CHAT FROM "[Freunde]" DO someAction
END
```
:::


Kompliziert wird es, wenn man die Reihenfolge nicht beachtet (diese ist IMMER `WHO/TYPE/ROOM`, auch wenn man im Quellcode diese Angaben umstellen darf!).

::: details Beispiel
```
WHEN KEYWORD x FROM who IN ROOM room DO action
WHEN KEYWORD x FROM * TYPE GTELL DO action
# Für den User "who" wird IMMER nur das erste WHEN beachtet, weil
# dieser User Spezialbehandlung geniesst. Selbst, wenn "room" ein
# Kanal (GTELL) ist, passiert nichts! Dazu braucht es:
WHEN KEYWORD x FROM who TYPE GTELL DO action
# Auf der anderen Seite wird für diesen User "who" neben dem ersten
# WHEN auch noch dieses hier ausgewertet:
WHEN KEYWORD x TYPE GTELL DO action
# Warum ist dies besonders? Naja, nimmt man alle FROMs raus, dann
# würde dies nicht passieren. Dieses letzte WHEN würde z.B. nicht
# ausgeführt werden, gäbe es ein
WHEN KEYWORD x IN ROOM room DO action
# weil es dann eine Sonderbehandlung für "GTELL" gäbe und die
# Behandlung aller TYPEs (mit Namen "room") entfallen würde.
```

> Quelle: BSW Puppet Forum >> Einige Detail-Fragen zur Puppet-Programmierung - Antwort #59
:::
