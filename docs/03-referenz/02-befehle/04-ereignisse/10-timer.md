# TIMER-Ereignis

## WHEN TIMER

```
WHEN TIMER <sek> DO <action>
```

- Alle \<sek> Sekunden wird die Action \<action> ausgeführt.


::: warning Warnung
Das Puppet kann sich nur einen Timer merken.
:::



## IGNORE TIMER

```
IGNORE TIMER
```

- Der Timer wird abgeschaltet.
