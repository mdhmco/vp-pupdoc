# OWNED-Ereignis

## WHEN OWNED

```
WHEN OWNED [FROM <liste>] DO <action>
```

- Wenn jemand von \<liste> das Puppet owned, wird die Action \<action> ausgeführt.
  

::: details Liste der belegten Variablen
* `WHO` = Person, die das Puppet owned
:::


:gear: Siehe auch Kapitel [Einstellungen für Puppetowning &raquo; NEWOWNER](../../../02-basic/03-einstellungen#einstellungen-fur-puppetowning)



## IGNORERR OWNED

```
IGNORE OWNED [FROM <liste>]
```

- Die Leute von \<liste> werden nicht mehr beim ownen beachtet.
- Fehlt die Liste ganz, so wird überhaupt nicht mehr auf ownen geachtet.
