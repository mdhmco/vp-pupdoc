# CHAT-Ereignis

## WHEN CHAT

```
WHEN CHAT [erweiterte WHEN Bedingungen] DO <action>
```

- Das Puppet reagiert auf die Ausgabe von Text, indem die angegebene Action aufgerufen wird.
- Die Wirkungsweise des Befehls kann durch [Erweiterte WHEN Bedingungen](./01-info-erwbed#erweiterte_when_bedingungen) eingeschränkt werden.


::: details Liste der belegten Variablen
* `WHO` = Sprecher des Textes
* `CHAT` = Eingegebener Text
* `TYPE` = Art des Chats (:gear: Siehe [Variable TYPE](../../04-variablen/02-var_befehle#variable-type))
* `ROOM` = Aufenthaltsort des Sprechers (:gear: Siehe [Variable ROOM](../../04-variablen/02-var_befehle#variable-room))
* `ROOMNAME` = Angabe zum Aufenthaltsort des Sprechers (:gear: Siehe [Variable ROOMNAME](../../04-variablen/02-var_befehle#variable-roomname))
:::


::: details Beispiel
```
ACTION eineAction
  # Registrierung diverser Actions
  #  - Reaktion auf Textausgaben von einer bestimmten Person
  WHEN CHAT FROM "Kugelschreiber" DO kuliAction
  #  - Reaktion auf Textausgaben von Personen, die in einer Liste stehen
  WHEN CHAT FROM "[Freunde]" DO freundeAction
  #  - Reaktion auf Textausgaben von Personen, zu denen sonst keinen Eintrag haben
  WHEN CHAT FROM "*" DO sonstAction
END
```
:::



## IGNORE CHAT

```
IGNORE CHAT [erweiterte WHEN Bedingungen]
```

- Das Puppet ignoriert ab sofort die Ausgabe von Text.
- Die Wirkungsweise des Befehls kann durch [Erweiterte WHEN Bedingungen](./01-info-erwbed#erweiterte_when_bedingungen) eingeschränkt werden.


::: details Beispiel
```
ACTION eineAction
  # De-Registrierung diverser Actions
  #  - Ignoriert Textausgaben einer bestimmten Person
  IGNORE CHAT FROM "Kugelschreiber"
  #  - Ignoriert Textausgaben der Personen, die in einer Liste stehen
  IGNORE CHAT FROM "[Freunde]"
  #  - Ignoriert alle Textausgaben
  IGNORE CHAT
END
```
:::



## IGNOREALL CHAT

```
IGNOREALL CHAT [erweiterte WHEN Bedingungen]
```

- Das Puppet ignoriert ab sofort die Ausgabe von beliebigem Text.
- Die Wirkungsweise des Befehls kann durch [Erweiterte WHEN Bedingungen](./01-info-erwbed#erweiterte_when_bedingungen) eingeschränkt werden.
