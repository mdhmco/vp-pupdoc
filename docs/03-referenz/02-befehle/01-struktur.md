# Befehle zur Puppetstruktur

## DO-Befehl

```
DO <action>
```

* Die Abarbeitung der Befehle der aktuellen Action wird unterbrochen und die Befehle der angegebenen Action ausgeführt.


::: details Beispiel
```
ACTION rufendeAction
  # Die Befehle werden in angegebener Reihenfolge ausgeführt:
  #  - Ausführung an erster Stelle
  DO gerufeneAction
  #  - Ausführung an dritter Stelle
END
ACTION gerufeneAction
  # Diese Befehle werden aufgrund des Aufrufs zwischengeschoben:
  #  - Ausführung an zweiter Stelle
END
```
:::



## IF-Befehl

```
IF <ausdruck>
  <befehl>
[ELSE
  <befehl>]
#oder
IF <ausdruck>
BEGIN
  <befehle>
END
[ELSE
BEGIN
  <befehle>
END]
```

* Liefert \<ausdruck> `TRUE` zurück, so wird der/die Befehl/e ausgeführt.
* Falls der `ELSE`-Teil angegeben ist, wird der/die Befehl/e dort ausgeführt, falls \<ausdruck> nicht `TRUE` ist.
* Bei verschachtelten IF/ELSE-Anweisungen kann es passieren, dass nicht klar ist, zu welchem `IF` ein `ELSE` gehört. Hier wird dann immer das äußerste `IF` genommen.


::: warning Warnung
Ein `IF` mit `ELSE` darf nie dort stehen, wo nur ein einzelner Befehl erlaubt ist (z.B. im Rumpf einer Schleife oder ein Zweig eines `IF`). In solchen Fällen muss immer mit `BEGIN ... END` geklammert werden.
:::


::: details Beispiel
```
ACTION eineAction
  # Test, ob ein bestimmter Benutzer angemeldet ist.
  IF [WHO] == "OneNick"
  BEGIN
    #  - Befehle für diesen einen Benutzer
  END
  ELSE
  BEGIN
    #  - Befehle für alle anderen Benutzer
  END
END
```
:::



## SWITCH-Befehl

```
SWITCH <expr>
  CASE <matchexpr1>
  [CASE <matchexpr2>]
    <befehl>
    ...
  END
  CASEALL <matchexpr3>
  [CASEALL <matchexpr4>]
    DO <action>
  [CASE "*"
    <befehl>
    ...
  END]
  [CASEALL "*"
    <befehl>
    ...
  END]
END
```

* `<expr>` ist ein Ausdruck, kann z.B. auch `LISTLENGTH [CHAT]` sein.
* `<matchexpr1-4>` ist ebenfalls ein Ausdruck, mit dem das (feste) Ergebnis des `SWITCH`-Ausdrucks mittels `MATCHES` verglichen wird.
* In allen Zweigen ist der `DO`-Aufruf und die durch ein `END` beendete Auflistung der Befehle möglich.


*Erläuterung der auszuführenden Zweige:*

* `CASE "<matchexpr1>"`
  - Die verschiedenen `SWITCH`-Ausdrücke werden getestet. Es wird der erste Zweig durchlaufen zu dem eine Übereinstimmung festgestellt wird.
* `CASEALL "<matchexpr3>"`
  - Wenn der `SWITCH`-Ausdruck übereinstimmt, wird dieser Zweig durchlaufen - auch wenn bereits ein vorheriges `CASE` / `CASEALL` zutraf.
* `CASEALL "*"`
  - Dieser Zweig wird auf jeden Fall ausgeführt.
* `CASE "*"`
  - Dieser Zweig wird ausgeführt, wenn vorher keiner der anderen `CASE` / `CASEALL` zutraf.

> Quelle: BSW Puppet Forum >> Puppet Dokumentation vom Kugelschreiber - Antwort #17 und >> Switch case else - Antwort #5


::: details Beispiel#1
```
ACTION eineAction
  WHEREIS PUPPET
  SWITCH [ROOM]
    CASE "C*-*-*"
      >> /tell [OneNick] [ROOM] ist in einer Burg/Kathedrale oder im Olympiastadion
    END
    CASE "C*-*"
      >> /tell [OneNick] [ROOM] ist der Spielraum der Stadt C[SUBST1]
    END
    CASE "C*"
      >> /tell [OneNick] [ROOM] ist der Marktplatz der Stadt C[SUBST1]
    END
    CASE 0
      DO boerse
    CASE 28
    END
    CASE 43
      DO manager
    CASE "*"
      >> /tell [OneNick] [ROOM] ist ein Raum in ARMfeld
    END
    >> /tell [OneNick] Teile: [SUBSTLEN] - "[SUBST1]" "[SUBST2]" "[SUBST3]"
  END
END
```
> Quelle: Puppet TestSwitch - von SLC
:::


::: tip Tip
Man beachte, daß `matchexpr` nur einen Teilstring des Switch-Ausdrucks matchen muss. Ist dies nicht gewünscht, so gibt man `SWITCH "|[var]|" ... CASE "|[match]|"` an, wobei "|" hier ein beliebiges, sonst nicht vorkommendes Zeichen ist.
:::


::: details Beispiel#2
```
SWITCH "|[maximum]|"
  CASE "|[wert1]|"
    SET listennummer 1
  END
  CASE "|[wert2]|"
    SET listennummer 2
  END
  CASE "|[wert3]|"
    SET listennummer 3
  END
END
```
> Quelle: BSW Puppet Forum >> Puppet für Mara gesucht - Antwort #57 (ff.)
:::



## WHILE-Befehl

```
WHILE <ausdruck>
  <befehl>
#oder
WHILE <ausdruck>
BEGIN
  <befehle>
END
```

* Solange \<ausdruck> `TRUE` liefert, wird der/die Befehl/e ausgeführt.


::: details Beispiel
```
ACTION eineAction
  # Es wird eine Zählschleife abgebildet (1..100)
  SET zaehler 1
  WHILE [zaehler] <= 100
  BEGIN
    #  - Befehle für den aktuellen Zählerwert ...
    #  - Zählerwert erhöhen
    EVAL zaehler = [zaehler] + 1
  END
END
```
:::


::: warning Warnung
Zählschleifen sollten nach Möglichkeit mittels `FOR` umgesetzt werden, weil jene mit `WHILE` aufgrund der zusätzlichen Statements signifikant langsamer ablaufen.
:::



## FOR-Befehl

### Zählschleifen

```
FOR <varname> = <von> TO <bis>
BEGIN
  <befehle> # bzw. <befehl>
END
#
FOR <varname> = <von> TO <bis> STEP <schritt>
BEGIN
  <befehle> # bzw. <befehl>
END
# oder
FOR <varname> = <von> DOWNTO <bis>
BEGIN
  <befehle> # bzw. <befehl>
END
#
FOR <varname> = <von> DOWNTO <bis> STEP <schritt>
BEGIN
  <befehle> # bzw. <befehl>
END
```


::: details Beispiel
```
ACTION eineAction
  # Es werden die Zahlenwerte 1,...,10 durchlaufen
  FOR i = 1 TO 10
  BEGIN
    # ...
  END
  # Es werden die Zahlenwerte 1,4,7,10 durchlaufen
  FOR i = 1 TO 10 STEP 3
  BEGIN
    # ...
  END
  # Es werden die Zahlenwerte 10,...,1 durchlaufen
  FOR i = 10 DOWNTO 1
  BEGIN
    # ...
  END
  # Es werden die Zahlenwerte 10,7,4,1 durchlaufen
  FOR i = 10 DOWNTO 1 STEP 3
  BEGIN
    # ...
  END
END
```
:::


:gear: Für weitere Erläuterungen siehe Kapitel [Ausdrücke für Listen](../03-ausdruecke/05-listen) -- und zwar Ausdrücke mittels derer Listen erzeugt werden.


### Zählschleifen unter Zuhilfename von Zähler-Listen

```
FOR <varname> IN <von> TO <bis>
BEGIN
  <befehle> # bzw. <befehl>
END
#
FOR <varname> IN <von> TO <bis> STEP <step>
BEGIN
  <befehle> # bzw. <befehl>
END
# oder
FOR <varname> IN <von> DOWNTO <bis>
BEGIN
  <befehle> # bzw. <befehl>
END
#
FOR <varname> IN <von> DOWNTO <bis> STEP <step>
BEGIN
  <befehle> # bzw. <befehl>
END
```


::: details Beispiel
```
ACTION eineAction
  # Es werden die Zahlenwerte 1,...,10 durchlaufen
  FOR i IN 1 TO 10
  BEGIN
    # ...
  END
  # Es werden die Zahlenwerte 1,4,7,10 durchlaufen
  FOR i IN 1 TO 10 STEP 3
  BEGIN
    # ...
  END
  # Es werden die Zahlenwerte 10,...,1 durchlaufen
  FOR i IN 10 DOWNTO 1
  BEGIN
    # ...
  END
  # Es werden die Zahlenwerte 10,7,4,1 durchlaufen
  FOR i IN 10 DOWNTO 1 STEP 3
  BEGIN
    # ...
  END
END
```
:::


:gear: Für weitere Erläuterungen siehe Kapitel [Ausdrücke für Listen](../03-ausdruecke/05-listen) -- und zwar Ausdrücke mittels derer Listen erzeugt werden.


### Konstrukte zur Abarbeitung von Listen

```
FOR <entry> IN <liste>
  <befehl>
# oder
FOR <entry> IN <liste>
BEGIN
  <befehle>
END
```

* Für alle Elemente von \<liste> wird der/die Befehl/e ausgeführt. Dabei enthält die Variable \<entry> das entsprechende Element.


::: details Beispiel#1
```
ACTION eineAction
  SET personen "Jupp Jeck Jacky"
  #
  FOR person IN [personen]
  BEGIN
    # Befehle für eine aktuelle Person (Zugriff mittels [person])
  END
END
```
:::


::: details Beispiel#2
```
ACTION eineAction
  # Es werden bestimmte Elemente aus einem String verarbeitet.
  SET text "aa bb ccc dd eee ff g hh"
  #
  FOR i IN "2 5 7"
  BEGIN
    EVAL element = [i] ELEMENTOF [text]
    # Element verarbeiten ... (bb, eee, g)
  END
END
```
:::


## RETURN-Befehl

```
  RETURN
```

* Verlässt vorzeitig die aktuelle Action.
