# Befehle für Textausgabe


## Offene Textausgabe

```
>> <text>
```

* Der \<text> wird vom Puppet ausgegeben.
* Bei der Ausgabe in einem Tell- oder Channel-Fenster werden mehrfache Leerzeichen gekürzt, im Hauptchat jedoch nicht.
* Wenn Text mit einem Slash (/) beginnt, so wird der entsprechende BSW-Befehl ausgeführt. Siehe hierzu die [BSW-Kommandos](./11-cmdausf#bsw-kommandos).


::: details Beispiel
```
ACTION eineAction
  SET Test3 "1 Punkt"
  SET Test3 [=WITHOUTFIRSTCHAR [Test3]]
    ## Neuer Inhalt von Test3: " Punkt"
    ## Beachte: Variableninhalt beginnt mit einem Leerzeichen
  >> Test3 *** [Test3]
    ## Ausgabe: "Test3 *** Punkt"
    ## Hier hat es 2 Leerzeichen zwischen '***' und 'Punkt'
  >> /tell Pepe-CH Test3 *** [Test3]
    ## Ausgabe: "*** Punkt"
    ## Hier hat es 1 Leerzeichen zwischen '***' und 'Punkt'
  >> /gtell PepeTest Test3 *** [Test3]
    ## Ausgabe: "*** Punkt"
    ## Hier hat es 1 Leerzeichen zwischen '***' und 'Punkt'
END
```
:::



## Gezielte Textausgabe

```
-- <text>
```

* Wenn eine Action durch [@-Befehle](../01-basic/04-addbefehle) ausgelöst wurde, bekommt nur der auslösende User den angegebenen "\<text>" als "--"-Meldung.
* Bei When-Events angewendet, benachrichtigt das Puppet den  auslösenden User `[WHO]` (analog zu [@-Befehle](../01-basic/04-addbefehle)).


::: details Beispiel @-Befehl
```
PUPPET beispiel
@list: list
ACTION list
  -- Ich bin leider nur ein Beispiel, aber ansonsten könntest du hier
  -- rausbekommen, was ich alles kann...
END
PUPPETEND
```
:::


Startet man dieses Puppet und gibt `@list` als Kommando ein, so erhält man den in der Action "list" angegebenen Text.


### Herkunftsangabe bei "--"-Ausgaben

Wird Text mit "--" ausgegeben, so ist auch interessant, wer dies von sich gibt. Die Ausgabe sieht z. B. so aus:

```
-- [PuppetConsole] Huhu, SLC, Du hast ein Ereignis ausgelöst...
```



## Versteckte Textausgabe

```
>> ;<empfaenger> <text>
```
