# Befehle für Daten


## SAVEDATA-Befehl

```
SAVEDATA
```

* Speichert die Inhalte der zur Verfügung stehenden SAVE-Variablen (`SAVE1` bis `SAVE[ __MAXSAVE]`) sofort.
* Andernfalls werden die Inhalte zum Puppetende gespeichert.
* Voraussetzung ist die Angabe von `SAVE:` unter den [Puppeteinstellungen](../01-basic/03-einstellungen).
* Diese SAVE-Variablen werden beim Programmstart mit den zuletzt gespeicherten Werten belegt. Damit sieht es so aus, als wären diese Variablen permanent. 


::: tip Tip
Für kritische Werte (z.B. in einer `WHEN KICKED` Action) sollte `SAVEDATA` explizit aufgerufen werden.
:::


*Verwendung des SAVEDATA-Befehls*

`SAVEDATA` soll nur in der KICKED-Action genutzt werden. Dort auch nur dann, wenn eine `SAVE` Variable geändert wird und danach noch Anweisungen kommen, die ggfs. mangels Online-Sein bzw. Server nicht mehr ausgeführt werden könnten.

Sonstige Benutzung zwingt den Server, zu einem bestimmten Zeitpunkt zu speichern, der eventuell nicht optimal ist. Dies führt zu einer hoher Gefahr des Datenverlustes.

> Quelle: BSW Puppet Forum >> Wer hat meine SAVE-Variablen gesehen - Antwort #10


::: details Beispiel
```
# ------------------------------------------------------------- kicked
ACTION kicked
  # --- Datum/Uhrzeit
  GETDATE
  # --- Speicherung des letzten Kickers
  SET SAVE99 "Kick durch [WHO] am [DAY].[MONTH] um [HOUR]:[MIN]"
  # --- ... alternativ: mit Verwendung einer Variablen für die SaveNummer
# SET _SaveLastStop_ 99 # (irgendwo anders im Puppet definiert...)
# SET SAVE[_SaveLastStop_] "Kick durch [WHO] am [DAY].[MONTH] um [HOUR]:[MIN]"
  # --- Speicherung der Daten
  SAVEDATA
END
```
:::


::: details SLC Beispiel aus dem Forum zur Anwendung von SAVE-Variablen
```
ACTION start
  SET SichereVariable SAVE1
END
...
SET [SichereVariable] 5
EVAL [SichereVariable] = [[SichereVariable]] + 9
...
```

Dann braucht man sich um nichts mehr zu kümmern (muß aber immer ein []-Paar mehr machen).
:::



*Verwendung von SAVE-Variablen durch zwei Puppets*

SAVEs werden nur beim Start geladen, aber an verschiedenen Stellen (insbesondere bei Veränderung, z.T. aber auch über einen Timer) gespeichert...

Aus diesem Grund empfiehlt SLC, zwei Puppets mit demselben SAVE-Bereich nacheinander und nie gleichzeitig zu starten. In diesem Fall sollte der Datenaustausch über die gemeinsam genutzten SAVE-Variablen zuverlässig sein. 



## SAVEPUBLIC-Befehl
-----------------

```
SAVEPUBLIC
```

* Speichert die Inhalte der Variablen `PUBLIC1` bis `PUBLIC100` in eine Textdatei, die man unter
  `http://213.155.73.107/Client/puppetSave/<puppetname>.data` finden kann.
* Hierbei wurden beim Puppetnamen alle Sonderzeichen automatisch durch "_" ersetzt.
* Der Inhalt der PUBLIC-Variablen steht beim nächsten Programmstart genauso wie derjenige der SAVE-Variablen unverändert zur Verfügung. 


::: tip Hinweis
Oftmals wird dies zur Generierung einer HTML Seite genutzt, die dann im Browser Ergebnisse des Puppets dargestellt. Hierbei erzeugen leere PUBLIC's ("") einen Zeilenumbruch, nicht vorhandene (`UNSET`) aber nicht.
:::

> Quelle: BSW Puppet Forum >> GURU - Best Practices der Puppet-Programmierung - Antwort #10
