
# Befehle für Informationen


## Anmerkungen bzgl. Rückgabewerte

::: tip Hinweis
Bitte beachten, dass Rückgabewerte von Befehlen wie `GETINFO`, `GETBOERSENINFO` usw. "local" sind.
:::


"Local" heisst, dass Werte wie `[RANK]`, `[KAUF]`, `[WHO]`, etc. zwar in Unteraufrufen vorhanden sind, nicht aber mehr bei Rücksprüngen aus Actions oder Begin/End-Blöcken. Hierfür müsste man eine globale Kopie anlegen.



## GETDATE-Befehl

```
GETDATE
```

* Liefert die aktuelle Uhrzeit und das aktuelle Datum zur Default-Zeitzone "CET".


```
GETDATE [<tz>]
```

* Liefert die aktuelle Uhrzeit und das aktuelle Datum zur angegebenen Zeitzone (Beispiel: "CET", "GMT+1" (=Winterzeit), "GMT+2" (=Sommerzeit), ... )


::: details Liste der belegten Variablen
* `SEC` = Sekunden
* `MIN` = Minuten
* `HOUR` = Stunden
* `DAY` = Tag im Monat
* `MONTH` = Monat (als Zahl)
* `YEAR` = Jahr
* `DAYOFWEEK` = Wochentag (Sonntag=1, Montag=2, ..., Samstag=7)
* `TIMEZONE` = entspr. Java-Rückgabe (Bsp: "Zentraleuropäische Zeit")
* `BSWDAY` = Lfd. Tag 1, ..., 60 der Jahreszeit
* `BSWSEASON` = Lfd. Nr. 1, 2, 3, 4 der Jahreszeit
* `BSWSEASONNAME` = "Lenzing", "Ernting", "Gilbhart", "Hartung" (oder deren Übersetzungen!)
* `BSWYEAR` = 240-Tage-Jahre seit Datenbankeinführung 
:::


::: details Beispiel
```
# -------------------------------------------------------------- error
ACTION error
  # --- Datum/Uhrzeit
  GETDATE
  IF [HOUR] < 10
    SET HOUR "0[HOUR]"
  IF [MIN] < 10
    SET MIN "0[MIN]"
  IF [SEC] < 10
    SET SEC "0[SEC]"
  # --- Meldung setzen
  SET SAVE[_SaveLastStop_] "Fehler am [DAY].[MONTH] um [HOUR]:[MIN] -- Meldung: [MESSAGE]"
  # --- Daten sichern
  SAVEDATA
END
```
:::



## GETINFO-Befehl

```
GETINFO <name>
```

* Liefert zum Spieler \<name> eine Reihe von Informationen. 
* Voraussetzung: Die Person muss in der BSW anwesend sein.
* Ist dies nicht der Fall, so werden die Variablen generell gelöscht. Ausnahme dürfte die Variable `ONLINE` sein.


::: details Liste der belegten Variablen
* `AMT` = Amt des Spielers
* `CITY` = Name der Stadt des Spielers
* `CITYNR` = Nummer der Stadt
* `GILDE` = Gildennummer der Gilde (0 = keine Gilde)
* `GILDENAMT` = Gildenamt des Spielers, falls `[GILDE]` > 0
* `GILDENNAME` = Name der Gilde, falls `[GILDE]` > 0
* `GILDENRANK` = Gildenrang (0, 1, ..., 5), falls `[GILDE]` > 0
* `LANGUAGE` = z. B. de/en je nach Einstellung
* `NAME` = Name des Spielers (evtl. umbenannt)
* `ONLINE` = Angabe, ob der Spieler online ist
* `ORGNAME` = Anmelde-Name des Spielers
* `PUPPET` = `TRUE`, falls es sich um ein Puppet handelt
* `PLAYING` = `TRUE`, falls der Spieler ein Spiel spielt (oder peekt)
* `PEEKING` = `TRUE`, falls die Person einem Spieler in die Karten schaut (peekt)
* `RANK` = Rang des Spielers
* `REPORTER` = `TRUE`, falls der Spieler ein Reporter ist, sonst `FALSE`
* `SEX` = n/m/w je nach Geschlecht des Spielers (Puppets und Bettler sind n)
* `TITEL` = Titel des Spielers
* `TUTOR` = `TRUE`, falls der Spieler ein Tutor ist, sonst `FALSE`
:::


::: details Beispiel
```
ACTION eineAction
  # --- Information zur anwesenden Person ermitteln
  GETINFO [anwesender]
  # Online und kein Puppets?
  IF [ONLINE] AND  NOT [PUPPET]
    IF [ORGNAME] IN [_freunde_]
      >> ;[anwesender] Hallo [ORGNAME], ich grüsse dich!
  # Seit Umstellung der Auswertung von Ausdrücken auf "short circuit"
  # sollte auch dies (alternativ) möglich sein:
  IF [ONLINE] AND (NOT [PUPPET]) AND ([ORGNAME] IN [_freunde_])
    >> ;[anwesender] Hallo [ORGNAME], ich grüsse dich!
END
```
:::


*Hinweise zur Auswertung der Informationen*

Die Reihenfolge der hier gezeigten Abfragen ist empfehlenswert. Zwischen der Ermittlung der anwesenden Personen und damit der Belegung der Variablen "anwesender" und dem Aufruf von `GETINFO` kann sich diese Person abgemeldet haben. 

Bevor auf weitere Werte zurückgegriffen wird, sollte erst der Wert der Variablen `ONLINE` getestet werden. Ansonsten kann es zu Fehlern aufgrund z.B. fehlerhafter boolscher Werte (bei Test von `[PUPPET]`) kommen.



## GETWHO-Befehl

```
GETWHO
```

* Liefert eine Liste der anwesenden Personen.


::: details Liste der belegten Variablen
* `WHO` = Liste aller im Raum anwesenden Spieler
:::


::: details Beispiel
```
ACTION eineAction
  # Anwesende Personen ermitteln
  GETWHO
  # Alle anwesende Personen verarbeiten
  FOR anwesender IN [WHO]
  BEGIN
    # --- Die einzelnen Anwesenden stehen der Reihe nach in der
    # --- Variablen "anwesender".
  END
END
```
:::



## GETROOMINFO-Befehl

```
GETROOMINFO
```

* Liefert diverse Informationen über den Raum, in dem sich das Puppet befindet.


::: details Liste der belegten Variablen
* `ROOM` = Nummer des Raumes
* `NAME` = Name des Raumes
* `OWNER` = Besitzer des Raumes, im Falle eines Wohnhauses werden die Bewohner in einer Liste geliefert
* `GAME` = Name des Spiels in dem Raum. Inhalte:
  - "BaseInformer" = Nicht-spezialisierter Informer
  - "--" = Residenz ohne Spiel
  - \<Spielname> = Residenz/Raum mit entspr. Spiel
  - "Baustelle", "Gildenhaus", "Hanse", "Labor", "Lager", "Manager", "Schatzkammer", "Schmiede", "Weberei", "Wohngebäude" = Raum mit entspr. Funktion.
* `GAMENAME` = Name des Spiels in dem Raum. Abweichend von `GAME` bei den Spielen, die in der BSW nicht mehr verfügbar sind.
* `ACTIVE` = Gibt an, ob der Raum aktiv ist (`TRUE` / `FALSE`).
:::


Im Vorraum einer Burg/Kathedrale ("Cxx-yy") wird eine Liste von 3 Spielen geliefert.


::: tip Hinweis
Sowohl `GAME` als auch `OWNER` sind nicht vorhanden (`NOT EXISTS`), wenn es diese Angaben nicht gibt. Diese Felder sind dann also nicht nur leer (`ISEMPTY [...]`). 

> Quelle: BSW Puppet Forum >> Reset Puppet - Besonderheit Burg GETROOMINFO - Antwort #18
:::


::: details Beispiel
```
ACTION eineAction
  # Informationen aktueller Raum
  GETROOMINFO
  # Information ausgeben
  >> Das Puppet befindet sich in Raum [ROOM] mit Namen [NAME].
  >> Dort wird das Spiel [GAME] angeboten.
END
```
:::



*Besonderheiten bei der Variablen GAME*

Alternative Schreibweisen (für dasselbe Spiel) - vermutlich nicht vollständig:
```
REPLACELIST patchGAME "CCJuS" "CC-JaegerUndSammler"
REPLACELIST patchGAME "KUK" "Kardinal&Koenig"
REPLACELIST patchGAME "Mahjongg" "MahJongg"
REPLACELIST patchGAME "Packeis" "PackeisAmPol"
REPLACELIST patchGAME "SchutzDerBurg" "ImSchutzeDerBurg"
```


Spielersetzungen (nicht mehr existente Spiele wurden durch andere ersetzt) - vermutlich nicht vollständig:
```
REPLACELIST patchGAME "Alhambra" "ThurnUndTaxis"
#REPLACELIST patchGAME "Dominion" "Grimoria"
REPLACELIST patchGAME "FussballLigretto" "Pandemie"
REPLACELIST patchGAME "Heckmeck" "Atlantis"
REPLACELIST patchGAME "Metro" "SanktPetersburg"
REPLACELIST patchGAME "Niagara" "StoneAge"
REPLACELIST patchGAME "LostCities" "Pandemie"
REPLACELIST patchGAME "Origo" "Wizard"
#REPLACELIST patchGAME "PuertoRico" "SaeulenDerErde"
REPLACELIST patchGAME "RA" "Pandemie"
REPLACELIST patchGAME "SchrilleStille" "StoneAge"
```



## GETBOERSENINFO-Befehl

```
GETBOERSENINFO
```

* An der Börse kann das Puppet den Befehl `GETBOERSENINFO` benutzen.


::: details Liste der belegten Variablen
* `KAUF` = Liste der Einkaufspreise
* `VERKAUF` = Liste der Verkaufspreise

Die Einkaufs- / Verkaufspreise stehen in den jeweiligen Variablen in der Reihenfolge "Holz", "Stein", "Erz", "Wolle", "Nahrung", "Tuch" und "Werkzeug", durch Leerstellen getrennt. 
:::


::: details Beispiel
```
ACTION boersenAction
  # --- Börse betreten
  >> room 0
  # --- Informationen zur Boerse holen
  GETBOERSENINFO
  # --- Börseninformationen: EK
  EVAL ekListe = [KAUF]
  EVAL ekHolz = 1 ELEMENTOF [KAUF]
  EVAL ekStein = 2 ELEMENTOF [KAUF]
  EVAL ekErz = 3 ELEMENTOF [KAUF]
  EVAL ekWolle = 4 ELEMENTOF [KAUF]
  EVAL ekNahrung = 5 ELEMENTOF [KAUF]
  EVAL ekTuch = 6 ELEMENTOF [KAUF]
  EVAL ekWerkzeug = 7 ELEMENTOF [KAUF]
  # --- Börseninformationen: VK
  EVAL vkListe = [VERKAUF]
  EVAL vkHolz = 1 ELEMENTOF [VERKAUF]
  EVAL vkStein = 2 ELEMENTOF [VERKAUF]
  EVAL vkErz = 3 ELEMENTOF [VERKAUF]
  EVAL vkWolle = 4 ELEMENTOF [VERKAUF]
  EVAL vkNahrung = 5 ELEMENTOF [VERKAUF]
  EVAL vkTuch = 6 ELEMENTOF [VERKAUF]
  EVAL vkWerkzeug = 7 ELEMENTOF [VERKAUF]
  # --- ...
END
```
:::


*Börsenpuppets*

Mittlerweilen stehen eine ganze Reihe von Puppets an der Börse herum. Diese belauschen den an der Börsen durchgeführten Handel. Einige Puppets geben diese Informationen über eigene Channels weiter. 

* Puppet `GURU` = Infos über Befehl `@info`
<!--
* Puppet `ÄgyptensBörsentool` = Infos über Befehl `@list`
-->



## WHEREIS-Befehl

```
WHEREIS PUPPET/OWNER/STARTER
```

* Liefert die Nummer des Raumes, in dem sich das Puppet, der Owner bzw. der Starter des Puppets befindet.
* `WHEREIS PUPPET` ersetzt den alten Befehl `GETROOM`.


::: details Liste der belegten Variablen
* `ROOM` = Nummer des Raumes, in dem sich die entsprechende Person bzw. das Puppet befindet
:::


::: details Beispiel
```
ACTION eineAction
  # --- Aufenthaltsort Puppet
  WHEREIS PUPPET
  SET puppetRoom [ROOM]
  # --- Aufenthaltsort Owner
  WHEREIS OWNER
  SET ownerRoom [ROOM]
  # --- Aufenthaltsort Starter
  WHEREIS STARTER
  SET starterRoom [ROOM]
END
```
:::



## WHOIS-Befehl

```
WHOIS PUPPET/OWNER/STARTER/REGSTARTER
```

Der Befehl liefert folgende Informationen:
* `PUPPET` = Name des Puppets
* `OWNER` = Name des Owners
* `STARTER` = Name des Puppet-Starters
* `REGSTARTER` = Liste der in der Registrierung durch das Kommando `/puppetdef` definierten Starter


::: details Liste der belegten Variablen
* `WHO` = Name der entsprechende Person/en
:::


::: details Beispiel
```
ACTION eineAction
  # --- Name Puppet
  WHOIS PUPPET
  SET puppetName [WHO]
  # --- Name Owner
  WHOIS OWNER
  SET ownerName [WHO]
  # --- Name Starter
  WHOIS STARTER
  SET starterName [WHO]
END
```
:::
