# Interne Befehle


Befehle, die nur intern verwendet werden:

`CLEAR ATUSER`
* Löscht den Zieluser für "--" (gab's früher nur für `@xyz`).

`SET ATUSER`
* Setzt den Zieluser für "--" (gab's früher nur für `@xyz`).

`SET DEBUGMODES`
* Setzt die @@debug-Parameter.

`STARTWHO`
* Schaut sich beim Start an, wer im Startraum ist.


> Quelle: BSW Puppet Forum >> Offene Fragen bzgl. Puppet-Dokumentation - Antwort #1
