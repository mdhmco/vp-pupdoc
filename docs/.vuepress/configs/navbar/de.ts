import type { NavbarConfig } from '@vuepress/theme-default'
import { version } from '../meta'

export const en: NavbarConfig = [
  {
    text: 'Allgemeines',
    link: '/01-allgemein/00-page.md',
  },
  {
    text: 'Grundlagen',
    children: [
      '/02-basic/02-aufbau.md',
      '/02-basic/03-einstellungen.md',
      '/02-basic/04-addbefehle.md',
      '/02-basic/05-sprachelemente.md',
      '/02-basic/06-actions.md',
			'/02-basic/20-variablen.md',
			'/02-basic/21-debugging.md',
			'/02-basic/22-anhang.md',
    ],
  },
  {
    text: 'Referenz',
    children: [
			{
				text: 'Puppeteinstellungen',
				link: '/03-referenz/01-einstell/00-page.md',
			},
			{
				text: 'Befehle',
				link: '/03-referenz/02-befehle/00-page.md',
			},
			{
				text: 'Ausdrücke',
				link: '/03-referenz/03-ausdruecke/00-page.md',
			},
			{
				text: 'Besondere Variablen',
				link: '/03-referenz/04-variablen/00-page.md',
			},
			{
				text: 'Weitere Infos',
				link: '/03-referenz/05-infos/00-page.md',
			},
			{
				text: 'Debugging',
				link: '/03-referenz/09-debugging/00-page.md',
			},
		],
	},
	{
    text: 'Version',
    children: [
			{
				text: `v${version}`,
				link: ''
			},
    ],
  },
]
