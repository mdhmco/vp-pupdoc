import type { SidebarConfig } from '@vuepress/theme-default'

export const en: SidebarConfig = {
	'/01-allgemein/': [
		{
			text: 'Allgemeines',
			children: [
				'/01-allgemein/00-page.md',
				'/01-allgemein/01-historie.md',
			],
		},
	],
	'/02-basic/': [
    {
      text: 'Grundlagen',
			children: [
				'/02-basic/02-aufbau.md',
				'/02-basic/03-einstellungen.md',
				'/02-basic/04-addbefehle.md',
				'/02-basic/05-sprachelemente.md',
				'/02-basic/06-actions.md',
				'/02-basic/20-variablen.md',
				'/02-basic/21-debugging.md',
				'/02-basic/22-anhang.md',
			],
		},
	],
	'/03-referenz/01-einstell/': [
		{
			text: 'Puppeteinstellungen',
      link: '/03-referenz/01-einstell/00-page.md',
			children: [
				'/03-referenz/01-einstell/01-puppettexte.md',
				'/03-referenz/01-einstell/02-puppetcode.md',
				'/03-referenz/01-einstell/03-puppetown.md',
				'/03-referenz/01-einstell/04-puppetverh.md',
				'/03-referenz/01-einstell/05-puppetdecl.md',
			],
		},
	],
	'/03-referenz/02-befehle/': [
		{
			text: 'Befehle',
      link: '/03-referenz/02-befehle/00-page.md',
			children: [
				'/03-referenz/02-befehle/01-struktur.md',
				'/03-referenz/02-befehle/02-steuerung.md',
				'/03-referenz/02-befehle/04-ereignisse/00-page.md',
				'/03-referenz/02-befehle/05-information.md',
				'/03-referenz/02-befehle/06-textausgabe.md',
				'/03-referenz/02-befehle/07-variablen.md',
				'/03-referenz/02-befehle/08-strings.md',
				'/03-referenz/02-befehle/09-listen.md',
				'/03-referenz/02-befehle/10-daten.md',
				'/03-referenz/02-befehle/11-cmdausf.md',
				'/03-referenz/02-befehle/12-debugging.md',
				'/03-referenz/02-befehle/13-intern.md',
			],
		},
	],
	'/03-referenz/02-befehle/04-ereignisse/': [
		{
			text: 'Befehle für Ereignisse',
      link: '/03-referenz/02-befehle/04-ereignisse/00-page.md',
			children: [
				'/03-referenz/02-befehle/04-ereignisse/01-info-erwbed.md',
				'/03-referenz/02-befehle/04-ereignisse/02-chat.md',
				'/03-referenz/02-befehle/04-ereignisse/03-keyword.md',
				'/03-referenz/02-befehle/04-ereignisse/04-match.md',
				'/03-referenz/02-befehle/04-ereignisse/05-click.md',
				'/03-referenz/02-befehle/04-ereignisse/06-newroom.md',
				'/03-referenz/02-befehle/04-ereignisse/07-appear.md',
				'/03-referenz/02-befehle/04-ereignisse/08-disappear.md',
				'/03-referenz/02-befehle/04-ereignisse/09-time.md',
				'/03-referenz/02-befehle/04-ereignisse/10-timer.md',
				'/03-referenz/02-befehle/04-ereignisse/11-error.md',
				'/03-referenz/02-befehle/04-ereignisse/12-kicked.md',
				'/03-referenz/02-befehle/04-ereignisse/13-ping.md',
				'/03-referenz/02-befehle/04-ereignisse/14-owned.md',
				'/03-referenz/02-befehle/04-ereignisse/15-unowned.md',
			],
		},
	],
	'/03-referenz/03-ausdruecke/': [
		{
			text: 'Ausdrücke',
      link: '/03-referenz/03-ausdruecke/00-page.md',
			children: [
				'/03-referenz/03-ausdruecke/01-variablen.md',
				'/03-referenz/03-ausdruecke/02-boolsche.md',
				'/03-referenz/03-ausdruecke/03-zahlen.md',
				'/03-referenz/03-ausdruecke/04-strings.md',
				'/03-referenz/03-ausdruecke/05-listen.md',
				'/03-referenz/03-ausdruecke/06-sonstig.md',
				'/03-referenz/03-ausdruecke/07-typpruefung.md',
				'/03-referenz/03-ausdruecke/08-typumwand.md',
				'/03-referenz/03-ausdruecke/09-auswertung.md',
			],
		},
	],
	'/03-referenz/04-variablen/': [
		{
			text: 'Besondere Variablen',
      link: '/03-referenz/04-variablen/00-page.md',
			children: [
				'/03-referenz/04-variablen/01-var_umfeld.md',
				'/03-referenz/04-variablen/02-var_befehle.md',
			],
		},
	],
	'/03-referenz/05-infos/': [
		{
			text: 'Weitere Infos',
      link: '/03-referenz/05-infos/00-page.md',
			children: [
				'/03-referenz/05-infos/01-zeichensatz.md',
				'/03-referenz/05-infos/02-puppet-reg.md',
				'/03-referenz/05-infos/03-puppet-typen.md',
				'/03-referenz/05-infos/04-puppet-anzeige.md',
				'/03-referenz/05-infos/05-puppet-ausfuehrung.md',
				'/03-referenz/05-infos/06-puppet-rahmenbed.md',
			],
		},
	],
	'/03-referenz/09-debugging/': [
		{
			text: 'Debugging',
      link: '/03-referenz/09-debugging/00-page.md',
			children: [
				'/03-referenz/09-debugging/01-allgemeines.md',
				'/03-referenz/09-debugging/02-dbgmodus.md',
				'/03-referenz/09-debugging/03-ausg_einstellungen.md',
				'/03-referenz/09-debugging/04-ausg_befehle.md',
				'/03-referenz/09-debugging/05-ausg_ausdruecke.md',
				'/03-referenz/09-debugging/06-beispiele.md',
			],
		},
	],
}
