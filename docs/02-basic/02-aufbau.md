# Aufbau eines Puppets

```
PUPPET <name>
  <Einstellungen und @-Befehle>
  <actions>
PUPPETEND
```

Vor `PUPPET` und nach `PUPPETEND` können noch beliebig viele Zeilen mit unformatierter Information stehen. Dies ist besonders dann hilfreich, wenn auf dem Webspace, der zur Speicherung des Puppetcode verwendet wird, Werbung eingeblendet wird.

Folgende drei Namen müssen bei einem Puppet nicht zwangsläufig übereinstimmen:

* Der Name des Puppets (der Name hinter `PUPPET`). Dieser wird beim Kommando `/info` hinter "Rang" angezeigt.
* Der Name, unter dem das Puppet in der BSW registriert ist, bzw. (im Falle von unregistrierten Puppets) aktuell ausgeführt wird. Dieser wird beim Kommando `/info` hinter "Name" angezeigt.
* Der Name der Datei auf dem Web-Space. Dieser ist nur in der Info zur Registrierung ersichtlich.
