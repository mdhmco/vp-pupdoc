# Variablen

## Variablenaufbereitung

Bei vielen String- und Listen-Funktionen wurde auf das kosmetische Entfernen von Leerzeichen verzichtet. Dadurch verändert sich die Struktur der Liste nicht ungewollt. 


### Trimmen bei Variablenauswertung

* Man kann statt "[name]" auch Leerzeichen einfügen, etwa " [ name ]".
* Wird dies so gemacht, so wird angenommen, daß die Leerzeichen vorne und hinten beim Inhalt dieser Variablen nicht mitgeliefert (=entfernt) werden sollen. 


::: details Beispiel
```
ACTION eineAction
  SET Test1 "1 Punkt" ## Neuer Inhalt von Test1: "1 Punkt"
  EVAL Test1 = WITHOUTFIRSTCHAR [Test1] ## Neuer Inhalt von Test1: " Punkt"
  EVAL Test1 = WITHOUTFIRSTCHAR [Test1] ## Neuer Inhalt von Test1: "Punkt"
  >> Test1 ***[Test1]
  ## Ausgabe: "Test1 ***Punkt"
  SET Test2 "1 Punkt" ## Neuer Inhalt von Test2: "1 Punkt"
  EVAL Test2 = WITHOUTFIRSTCHAR [ Test2 ] ## Neuer Inhalt von Test2: " Punkt"
  EVAL Test2 = WITHOUTFIRSTCHAR [ Test2 ] ## Neuer Inhalt von Test2: "unkt"
  >> Test2 ***[Test2]
  ## Ausgabe: "Test2 ***unkt"
END
```

::: tip Hinweis
Dies zeigt auch, dass erst die Variable getrimmt wird, und erst dann der Ausdruck `WITHOUTFIRSTCHAR` ausgeführt wird.
:::



## Variablenersetzung

* Kommt in einem der Parameter irgendeines Befehls ein eckiges Klammernpaar vor, so wird vor Ausführung des Befehls der Variablenname zwischen den Klammern durch dessen Wert ersetzt.
* Eckige Klammern können verschachtelt werden. 


::: details Beispiel
```
# --------------------------------------------------------------- init
ACTION init
  # --- Informationen einholen
  WHOIS STARTER
  SET _starter_ [WHO]
  # --- Namen für Nummern der Save-Variablen
  SET _SaveConfig_      10
  SET _SaveConfig_City_  1
  SET _SaveConfig_Home_  2
  # --- Save-Variable: Config
  IF EXISTS SAVE[_SaveConfig_]
  BEGIN
    EVAL _city_ = [_SaveConfig_City_] ELEMENTOF [SAVE[_SaveConfig_]]
    EVAL _home_ = [_SaveConfig_Home_] ELEMENTOF [SAVE[_SaveConfig_]]
    >> /tell [_starter_] Konfiguration: [SAVE[_SaveConfig_]]
    >> /tell [_starter_] Puppet ist konfiguriert für city=[_city_] und home=[_home_]
  END
  # ...
END
```
:::


Möchte man eckige Klammern nicht ersetzen lassen, so muss man sie durch einen Backslash (\\) vor der Klammer zu einer normalen Klammer machen. Einem Backslash muss man ebenfalls einen (weiteren) Backslash voranstellen. 


::: details Beispiel
```
ACTION eineAction
  SET y 1
  >> /tell Kugelschreiber y = 1 : [y]
  EVAL y = [y] + 2
  >> /tell Kugelschreiber y = \[y\] + 2 : [y]
END
```

*Zugehörige Ausgabe:*
```
kuli997: y = 1 : 1
kuli997: y = [y] + 2 : 3
```
:::



## Besondere Variablen

:gear: Siehe Referenz [Besondere Variablen](../03-referenz/04-variablen/00-page.md)
