# Puppeteinstellungen

Es gibt eine Reihe von Einstellungen, die schon vor dem Ausführen der ersten Action bekannt sein müssen. Diese werden direkt hinter dem `PUPPET` Befehl angegeben:

```
LOGIN: <string>
LOGOUT: <string>
APPEAR: <string>
DISAPPEAR: <string>
INFO: <string>
NATION: bsw/<lang>
CITYCHAT: YES/NO
SAVE: <phrase>
OWN: YES/NO
NEWOWNER: YES/NO
TWINDOW: YES/NO
CASESENSITIV: YES/NO
OVERFLOW: ERROR/HARAKIRI/IGNORE
ZEROINTVARS: YES/NO
DEBUG: <liste>
@<command>: <action>
CHARSET: <string>
```

:gear: Für weitere Infos siehe Referenz [Puppeteinstellugen](../03-referenz/01-einstell/00-page.md)
