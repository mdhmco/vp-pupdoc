# @-Befehle

```
@<command>: <action>
```

Immer dann, wenn jemand im Raum das @-Kommando \<command> eingibt, wird die angegebene Action \<action> aufgerufen.

Der Benutzer gibt ein @-Kommando in folgender Schema ein:

```
@<command>[:<puppetname>] <parameter>
```

* Wird \<puppetname> weggelassen, so bekommen alle Puppets im Raum das @-Kommando.
* Standardmäßig sollte das Puppet bei Eingabe des Befehls `@list` eine Liste der @-Befehle ausgeben, die es kennt. Hierfür kann der "--"-Befehl benutzt werden.

::: details Liste der belegten Variablen
* `PARAM` = Parameter des Kommandos.
* `WHO` = Person, die das Kommando ausgelöst hat.
:::



## Kommando @@sound

```
@@sound[:<puppetname>] on
@@sound[:<puppetname>] off
```

Das ist quasi ein Pseudo-@-Befehl, den jedes Simple-/Command-Puppet beherrscht. Er kann nur durch den Starter des Puppets ausgelöst werden.

* Ohne Angabe von \<puppetname> bezieht sich die Einstellung auf alle eigenen Puppets im Raum (bzw. auch auf den Channel/Tell).
* Mittels `@@sound on` wird die Ausgabe im Raum, wo das Puppet steht, scharf geschaltet.
* Anschliessend werden die Texte aus dem Raum in der Form `[<puppetname>] Spieler: Blah....` angezeigt.
* Auch die serverseitig produzierten Töne des Raums werden dann zusätzlich ausgegeben.



## Kommando @@debug

:gear: Siehe Kapitel [Debugging](./21-debugging).
