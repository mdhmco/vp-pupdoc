# Historie des Dokuments

## Dokumentation auf Basis von VuePress

* V1.51.x (09/2021 - 10/2021)
  - Geringfügige Überarbeitungen
  - Kleinere Korrekturen
  - Weitere Anpassungen der Struktur
    - Randeffekt: Zergliederung in übersichtlichere Teiltexte
  - Erweiterungen
    - Kapitel "BSW Kommandos": Zwei Einschränkungen bezüglich Autostartpuppets
    - Wiederaufnahme von Kapiteln, die scheinbar im Laufe der Zeit unabsichtlich abhanden gekommen waren.
    - Kapitel "Weitere Infos" ➞ "Puppet Rahmenbedingungen"

* V1.50.0 (08/2021 - 09/2021)
  - Umstellung auf **VuePress**
    - Anpassung der Struktur
    - Löschung von Sphinx-spezifischen Angaben
  - GitLab Projekt


## Dokumentation auf Basis von Sphinx

* V1.30.x und V1.40.x (09/2018 - 01/2019)
  - Geringfügige Überarbeitungen
  - Festlegung der Lizenzbedingungen
  - Umstellung auf **Sphinx**


## Dokumentation auf Basis von Pandoc

* V1.20 (07/2018)
  - Rück-Umstellung auf **Pandoc**


## Dokumentation auf Basis von DokuWiki

* V1.11 (09/2017)
  - Ergänzung zu “WHEN TIME”
* V1.10 (01/2017)
  - Spielersetzung “EinfachGenial” durch “Imhothep”.
* V1.01 (12/2016) - V1.03 (12/2016)
  - Geringfügige Überarbeitungen
  - Umstellung auf **DokuWiki**


## Dokumentation auf Basis div. Tools

* Dieser Teil der Historie wurde gelöscht!
* Erste Version des Dokuments in **2006**
