# Allgemeines zum Dokument

Die Motivation zur Erstellung dieses Dokuments war im Februar 2006 das Bestreben, auf eine aktuelle Befehlsliste der Puppet-Programmierung in der [Brettspielwelt](http://www.brettspielwelt.de) (=BSW) Zugriff zu haben. Seitdem sind einige Jahre vergangen und die Häufigkeit der Aktualisierung hat kontinuierlich nachgelassen.

Die Bedeutung von Puppets in der BSW ist seit Einführung der mobilen Version und damit der "Apps" stark zurückgegangen. Neuerungen gab es schon lange nicht mehr.


::: warning Eindringlicher Hinweis
Es handelt sich um *KEIN* offizielles Dokument der BSW!
:::



## Randbedingungen

Folgende Randbedingungen gelten für das Dokument:

- Das Dokument wird aktuell vom BSW-User "Kugelschreiber" erstellt.
- Im Dokument sind die Inhalte aus der offiziellen BSW Puppet-Hilfe sowie die sporadischen Informationen innerhalb des BSW Puppet-Forums enthalten.
- Das Dokument wurde unter GitLab in zwei Ausprägungen zur Verfügung gestellt.



## Dokumentation in verschiedenen Formaten

Dies ist ein weiterer Anlauf, die Dokumentation in einer Form bereitzustellen, die mit sehr reduziertem Aufwand erzeugt werden kann.

Mittlerweilen basiert diese Dokumentation auf [VuePress](https://vuepress.vuejs.org/).
