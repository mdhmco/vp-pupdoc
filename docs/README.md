---
home: true
title: Home
heroImage: /images/hero.png
actions:
  - text: Allgemeines
    link: /01-allgemein/00-page.html
    type: secondary
  - text: Grundlagen
    link: /02-basic/00-page.html
    type: primary
  - text: Referenz Befehle
    link: /03-referenz/02-befehle/00-page.html
    type: secondary
footer: Released under the MIT License | Copyright © 2006-present by Michael Dahmen
---
